<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HeaderFooterController extends Controller
{
    public function index()
    {
    $klien = env('ID_KLIEN');
    $data_header_footer = DB::table('klien')
        ->select('header_klien', 'footer_klien')
        ->where('id_klien', $klien)
        ->first();
    return view('admin.headerfooter', compact('data_header_footer'));
    }


    public function updateHeaderFooter(Request $request)
    {
        $klien = env('ID_KLIEN');
        DB::beginTransaction();

        try {

                $header_klien = $request->input('header_klien');
                $footer_klien = $request->input('footer_klien');
            
            $proses_input = DB::select("update klien set header_klien = '$header_klien',footer_klien= '$footer_klien' where id_klien ='$klien'");

            DB::commit();
            return response()->json([
                'status'    => 'success', 
                'toast'     => 'Data berhasil disimpan'
            ]);
        } catch (\Exception $e) {
            // dd($e);
            dd($e->getMessage());
            DB::rollback(); // something went wrong
            return response()->json([
                'status'    => 'error',
                'toast'     => 'Data gagal disimpan'
            ]);
        }
    }


}
