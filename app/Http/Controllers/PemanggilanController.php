<?php

namespace App\Http\Controllers;

use App\Models\Layanan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class PemanggilanController extends Controller
{
    public function index(Request $request)
    {
        $id_petugas = auth()->guard('pengguna')->user()->id_pengguna;
        // $id_petugas = $request->session()->get('id');
        // dd($id_petugas);
        $id = env('ID_KLIEN');
        // $klien_layanan_nama = DB::select("SELECT * FROM klien_layanan join where id_klien = '$id' and where id_pe");
        $klien_layanan_nama = DB::table('klien_layanan')
            ->join('pengguna', 'klien_layanan.id_pengguna', '=', 'pengguna.id_pengguna')
            ->where('pengguna.id_pengguna', '=', $id_petugas)
            ->get();
        // dd($klienLayanan);
        return view('pengguna.pemanggilan', compact('klien_layanan_nama'));
    }

    public function edit(Request $request, string $id_klien_layanan)
    {
        $date_now = date('Y-m-d');
        // dd($date_now);

        //antrian saat ini
        $antrian_now = DB::select('SELECT id_proses_antrian, no_antrian FROM proses_antrian JOIN antrian ON proses_antrian.id_antrian = antrian.id_antrian ORDER BY proses_antrian.id_proses_antrian DESC LIMIT 1');

        //get kode layanan
        $kode_layanan = DB::select("SELECT kode_layanan FROM klien_layanan WHERE id_klien_layanan = '$id_klien_layanan'");

        //get antrian dengan status belum dilayani
        $antrian_belum_dilayani = DB::select("SELECT proses_antrian.id_proses_antrian, antrian.no_antrian, antrian.nama_pengunjung, antrian.kode_layanan, antrian.id_klien_layanan, proses_antrian.status FROM antrian JOIN proses_antrian ON antrian.id_antrian = proses_antrian.id_antrian WHERE proses_antrian.status = 'Belum Dilayani' AND id_klien_layanan = '$id_klien_layanan' AND proses_antrian.tanggal = '$date_now' ORDER BY no_antrian ASC LIMIT 1");

        //ambil data antrian
        $data_antrian = DB::select("SELECT antrian.nama_pengunjung, antrian.kode_layanan, antrian.no_antrian, antrian.id_klien_layanan, proses_antrian.status, proses_antrian.tanggal, proses_antrian.waktu FROM antrian JOIN proses_antrian ON antrian.id_antrian = proses_antrian.id_antrian WHERE id_klien_layanan = '$id_klien_layanan' AND proses_antrian.tanggal = '$date_now' AND status = 'Belum Dilayani' ORDER BY no_antrian ASC");
        $data_antrian_dilewati = DB::select("SELECT antrian.nama_pengunjung, antrian.kode_layanan, antrian.no_antrian, antrian.id_klien_layanan, proses_antrian.status, proses_antrian.tanggal, proses_antrian.waktu FROM antrian JOIN proses_antrian ON antrian.id_antrian = proses_antrian.id_antrian WHERE id_klien_layanan = '$id_klien_layanan' AND proses_antrian.tanggal = '$date_now' AND status = 'Dilewati' ORDER BY no_antrian ASC");

        //ambil layanan
        $layanan = DB::select("SELECT * FROM klien_layanan WHERE id_klien_layanan = '$id_klien_layanan'");

        return view('pengguna.formpemanggilan', compact('layanan', 'antrian_belum_dilayani', 'data_antrian', 'data_antrian_dilewati'));
    }

    public function update_status(Request $request)
    {
        $time_now = date('h:i:s');
        //proses update status ke selesai
        $proses_update_status = DB::select("UPDATE proses_antrian SET status = 'Selesai', updated_at = '$time_now' WHERE id_proses_antrian = '$request->id_antrian'");
        return response()->json($proses_update_status);
    }

    public function status_dilewati(Request $request)
    {
        $update_status_dilewati = DB::select("UPDATE proses_antrian SET status = 'Dilewati' WHERE id_proses_antrian = '$request->id_antrian'");
        return response()->json($update_status_dilewati);
    }
}
