<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Termwind\Components\Dd;
use Yajra\DataTables\Facades\DataTables;
use Twilio\Rest\Client;
class AntrianPenggunaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    
     public function index(Request $request)
     {
        $id = env('ID_KLIEN'); 
        // dd( $id);
         if (request()->ajax()) {
             $sql = "SELECT klien_layanan.nama_layanan, antrian.kode_layanan, antrian.no_antrian, antrian.nama_pengunjung, 
                     antrian.no_telp_pengunjung, proses_antrian.tanggal, proses_antrian.waktu, proses_antrian.status 
                     FROM antrian 
                     JOIN klien_layanan ON antrian.id_klien_layanan = klien_layanan.id_klien_layanan 
                     JOIN proses_antrian ON antrian.id_antrian = proses_antrian.id_antrian where klien_layanan.id_klien = '$id'";
     
             if ($request->input1) {
                 $sql .= " AND antrian.id_klien_layanan = '{$request->input1}'";
             }
             
             if ($request->tanggal_start && $request->tanggal_end) {
                 $sql .= " AND proses_antrian.tanggal BETWEEN '{$request->tanggal_start}' AND '{$request->tanggal_end}'";
             }
             
             // Mengurutkan tanggal dari yang terbaru
             $sql .= " ORDER BY proses_antrian.tanggal DESC";
             
             $antrian = DB::SELECT($sql);
            //  dd($antrian);
             return DataTables::of($antrian)->make(true);
         }
         
         return view('pengguna/antrian_pengguna');
     }
     
     

    
    public function select2(){
        $data = DB::table('public.klien_layanan')->select('kode_layanan', 'nama_layanan', 'id_klien_layanan')->get();
        // dd($data);
        return $data;
    }




    
    public function insert(request $request)

    {
        //Insert untuk pengambilan antrian pada tabel antrian
        // $id = env('ID_KLIEN');
        // $antrian_getone = collect(DB::select('select id_klien_layanan from public.klien_layanan_getone(5)'))->first(); 
        // $klien_layanan_nama = DB::select('select * from public.klien_layanan_getall()');
        // $data_antrian = DB::select('select * from public.antrian_getall()');
        // return view('pengguna/antrian_pengguna', compact('antrian_getone'));
     

    }
    /**
     * Store a newly created resource in storage.
     */
    public function getone()
    {
        // $klien_layanan_nama = DB::select('select * from public.klien_layanan_getall()');
        // $antrian_getone = DB::select('select * from public.klien_layanan_getone(5)');
        // $data_antrian = DB::select('select * from public.antrian_getall()');
        // return view('pengguna/antrian_pengguna', compact('antrian_getone', 'klien_layanan_nama','data_antrian'));
    }

    /**
     * Display the specified resource.
     */
    public function store(request $request)
    {
        //Insert untuk pengambilan antrian pada tabel antrian
        // $id = env('ID_KLIEN');
        // $no_antrian = "A-009";
        // $antrian_getone = collect(DB::select('select id_klien_layanan from public.klien_layanan_getone(5)'))->first(); 
        // DB::select("select * from public.antrian_insert('$id', '$antrian_getone->id_klien_layanan', '$request->nama', '$request->nomor_telepon','$no_antrian')");
        // return redirect('antrian_pengguna');
    }

    

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

   
}
