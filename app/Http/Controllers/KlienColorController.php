<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KlienColorController extends Controller
{
    public function getAll()
    {
        $id_klien = env("ID_KLIEN");

        $list_data_klien_color = DB::select('SELECT * FROM public.klien_color_getall()');
        $list_data_klien_color_json = '{
                "status" : 0,
                "message" : "Success",
                "data":{
                    "id_klien" : 1,
                    "id_feedback" : 1,
                    "skor_aplikasi" : 9,
                    "skor_pelayanan" : 8,
                    "kritik_saran" : "Perlu ditingkatkan lagi",
                    "tanggal" : "2023-8-23"
        	},
            {
                "status" : -1,
                "message" : "Data tidak ditemukan",
                "data":null
            }
            }';
        // return view('klien_color.index', compact('list_data_klien_color'));
    }

    public function klien_color_getone()
    {
        $id_klien = env('ID_KLIEN');
        $data_klien_color_getone = DB::select("SELECT * FROM public.klien_color_getone(4)");

        // dd($data_klien_color_getone);

        return view('admin.klien_color', compact('data_klien_color_getone'));
    }

    public function insert($id_klien, $header_color, $footer_color, $text_color, $button_text_color, $button_color)
    {
        $insert_klien_color = DB::select('SELECT * FROM public.klien_color_insert($id_klien, $header_color, $footer_color, $text_color, $button_text_color, $button_color)');
        //penambahan record pada tabel feedback
        $status_klien_color_json = '{
            "data":[
            {
                "status" : 0,
                "message" : "Successfully Added!"
            },
            {
                "status" : -1,
                "message" : "Error!"
            }
            ]}';
        return $status_klien_color_json;
    }

    public function update($id_klien, $id_klien_color)
    {
        $update_klien_color = DB::select('SELECT * FROM public.klien_color_update($id_klien, $id_klien_color)');
        //Perubahan data pada tabel klien_color
        $status_klien_color_json = '{
            "data":[
            {
                "status" : 0,
                "message" : "Successfully Updated!"
            },
            {
                "status" : -1,
                "message" : "Error!"
            }
            ]}';
        return $status_klien_color_json;
    }

    public function delete($id_klien_color)
    {
        $delete_klien_color = DB::select("SELECT * FROM public.klien_color_delete('$id_klien_color')");

        if (!$delete_klien_color) {
            return response([
                'status' => -1,
                'data' => 'Error'
            ]);
        } else {
            return response([
                'status' => 0,
                'data' => 'Data Berhasil Dihapus'
            ]);
        }
    }
}
