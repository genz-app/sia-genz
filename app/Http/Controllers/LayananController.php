<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Illuminate\Support\Facades\Validator;


class LayananController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $id = env('ID_KLIEN');
        $klien_layanan_nama = DB::select("SELECT * FROM klien_layanan WHERE id_klien = '$id' ORDER BY kode_layanan");
        return view('publik/layanan', compact('klien_layanan_nama'));
    }

   
    public function show(string $id_klien_layanan)
    {
        $layanan = DB::select("SELECT * FROM klien_layanan WHERE id_klien_layanan = '$id_klien_layanan'");
        return view('publik/antrian', compact('layanan'));
    }


    public function update(Request $request, $id_klien_layanan)
    {
        $jenis_pemanggilan = "pengumuman";
        $status = "Belum Dilayani";
        $id_pengguna = 10;
        $get_date = date('Y-m-d');
        $get_time = date('h:i:s');
        $date_now = date('Y-m-d');

        // dd($get_date);

        //get id_klien_layanan
        $id_layanan = $id_klien_layanan;

        //get data layanan
        $data_layanan = collect(DB::select("SELECT kode_layanan FROM klien_layanan WHERE id_klien_layanan = '$id_layanan'"))->first();

        $id = env('ID_KLIEN');
        $get_day_now = date('d');

        //get nomor antrian otomatis
        $no_antrian = DB::table('antrian')->select('no_antrian')
            ->where('kode_layanan', $data_layanan->kode_layanan)
            ->where('tanggal', $date_now)
            ->max('no_antrian') + 1;

            //validator nomor telepon
            $validator = Validator::make($request->all(), [
                'nomor_telepon' => 'required|string|max:14|min:10',
            ]);
        
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

        //proses insert antrian
        $proses_input_antrian = DB::select("select * from public.antrian_insert('$id','$id_layanan', '$data_layanan->kode_layanan', '$no_antrian', '$request->nama','$request->nomor_telepon', '$date_now')");

        //get id_antrian terakhir
        $get_id_antrian = DB::table('antrian')->select('id_antrian')->max('id_antrian');


        //proses memasukkan data ke tabel proses_antrian
        $proses_antrian = DB::select("SELECT * FROM public.proses_antrian_insert('$id', '$get_id_antrian', '$id_pengguna', '$jenis_pemanggilan', '$status', '$get_date', '$get_time')");

        //get data antrian
        $get_data_antrian = DB::table('antrian')
            ->select('id_antrian', 'nama_pengunjung', 'no_telp_pengunjung', 'kode_layanan', 'no_antrian')
            ->where('no_antrian', $no_antrian)
            ->where('kode_layanan', $data_layanan->kode_layanan)
            ->where('tanggal', $date_now)
            ->first();

        return view('publik.konfirmasi', compact('get_data_antrian'));
    }





    /**
     * Display the specified resource.
     */

    /**
     * Show the form for editing the specified resource.
     */
    public function edit()
    {
        // $layanan = DB::select("SELECT * FROM public.klien_layanan_getone('$id_klien_layanan')");
        // return view('publik/antrian', compact('layanan'));
    }

    /**
     * Update the specified resource in storage.
     */
    // public function update(Request $request, string $id)
    // {
    //     //
    // }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
