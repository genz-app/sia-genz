<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function profil()
    {
        $id_klien = env('ID_KLIEN');
        $dataklien = DB::table('klien')->where('id_klien', '=', $id_klien)->get();
        // dd($dataklien);
        return view('profil', compact('dataklien'));
    }

    public function updateProfil(Request $request)
    {
        DB::beginTransaction();

        try {
            $nama = $request->nama;
            $email = $request->email;
            $alamat = $request->alamat;
            $id_klien = env('ID_KLIEN');
            $proses_input = DB::select("UPDATE klien SET nama_klien='$nama', email='$email', alamat='$alamat' WHERE id_klien='$id_klien'");

            DB::commit();
            return response()->json([
                'status'    => 'success',
                'toast'     => 'Data berhasil disimpan'
            ]);
        } catch (\Exception $e) {
            dd($e);
            DB::rollback(); // something went wrong
            return response()->json([
                'status'    => 'error',
                'toast'     => 'Data gagal disimpan'
            ]);
        }
    }
}
