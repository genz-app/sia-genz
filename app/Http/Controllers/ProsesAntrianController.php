<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProsesAntrianController extends Controller
{
    /**
     * Display a listing of the resource.
     */


    public function getone()
    {
        $id = env('ID_KLIEN');
        // $headerklien = collect(DB::select("select header_klien from public.klien_getone('$id')"))->first();
        // $footerklien = collect(DB::select("select footer_klien from public.klien_getone('$id')"))->first();
        $dataklien = DB::select("select * from public.klien_getone('$id')");

        $antrian_getone =  DB::select('select * from public.antrian_getone(6)');
        // $no_antrian =  collect(DB::select('select no_antrian from public.antrian_getone(6)'))->first();
        $no_antrian_saat_ini = "A-08";

        // dd($no_antrian);
        return view('publik/proses_antrian', compact('dataklien', 'antrian_getone', 'no_antrian_saat_ini'));
        // return view ('prosesantrian', compact('no_antrian','dataklien', 'no_antrian_saat_ini'));
    }

    public function insert()
    {
        //Insert untuk menambah data pada tabel proses_antrian

        $id = env('ID_KLIEN');
        $antrian_getone =  collect(DB::select('select id_antrian from public.antrian_getone(6)'))->first();
        $pengguna_getone = collect(DB::select('select id_pengguna from public.pengguna_getone(10)'))->first();
        $jenis_pemanggilan = 'Chat';
        $status = 'Sudah Dikonfirmasi';
        $tanggal = '2023-09-04';
        $waktu = '11:26:00';
        $insertfunc = DB::select("select * from public.proses_antrian_insert('$id', '$antrian_getone->id_antrian', '$pengguna_getone->id_pengguna', '$jenis_pemanggilan', '$status', '$tanggal', '$waktu')");

        if (count($insertfunc)) {
            return response()->json([
                'status' => '0',
                'message' => 'Success!',
                'data' => $insertfunc
            ]);
        } else {
            return response()->json([
                'status' => '-1',
                'message' => 'Tidak ditemukan data',
                'data' => ''
            ]);
        }

        // dd($antrian_getone);
        return $insertfunc;
    }

    public function update()
    {
        //Update untuk mengupdate data proses antrian pada tabel proses_antrian

        $id = env('ID_KLIEN');
        $idproantrian =  collect(DB::select('select id_proses_antrian from public.proses_antrian_getone(9)'))->first();
        $antrian_getone =  collect(DB::select('select id_antrian from public.antrian_getone(6)'))->first();
        $pengguna_getone = collect(DB::select('select id_pengguna from public.pengguna_getone(10)'))->first();
        $jenis_pemanggilan = 'Pengumuman';
        $status = 'Belum Dikonfirmasi';
        $tanggal = '2023-09-04';
        $waktu = '08:22:00';
        $updatefunc = DB::select("select * from public.proses_antrian_update('$idproantrian->id_proses_antrian', '$id', '$antrian_getone->id_antrian', '$pengguna_getone->id_pengguna', '$jenis_pemanggilan', '$status', '$tanggal', '$waktu')");
        return $updatefunc;
    }

    public function delete()
    {
        //Delete untuk menghapus data proses antrian pada tabel proses_antrian
        $idproantrian =  collect(DB::select('select id_proses_antrian from public.proses_antrian_getone(11)'))->first();
        DB::select("select * from public.proses_antrian_delete('$idproantrian->id_proses_antrian')");

        $status_proses_antrian_json = '{
            "data": [
            {
                "status" : "0",
                "message" : "Berhasil dihapus"
            },
            {
                "status" : "-1",
                "message" : "Gagal dihapus"
            }
            ]}';
        // dd($idproantrian);
        // return $status_proses_antrian_json;
    }

    public function getAll($id_proses_antrian, $id_klien)
    {
        //getAll untuk mengambil semua data pada tabel proses_antrian

        $pagetall = DB::select('select * from public.proses_antrian_getone()');

        $list_proses_antrian_json = '{
            "status" : "0",
            "message" : "Berhasil ditambahkan",
            "data": 
                {
                    "id_proses_antrian" 	: "1",
                    "id_klien" 			: "1",
                    "id_antrian" 		: "1",
                    "id_pengguna"		: "P1",
                    "jenis_pemanggilan"	: "
                    "no_antrian"		: "A-10",
                    "tanggal" 			: "2023-08-23"
                    "waktu" 			: "11:10:30"
                    "created_at"		: "2023-08-23 11:10:30"
                },
                {
                    "id_proses_antrian" 	: "2",
                    "id_klien" 			: "2",
                    "id_antrian" 		: "2",
                    "id_pengguna"		: "P1",
                    "jenis_pemanggilan"	: "
                    "no_antrian"		: "A-11",
                    "tanggal" 			: "2023-08-23"
                    "waktu" 			: "11:22:30"
                    "created_at"		: "2023-08-23 11:22:30"
                }
            }';
        return $list_proses_antrian_json;
    }

    public function kirimViaWa($id_proses_antrian, $id_klien, $id_antrian, $id_pengguna)
    {
        //kirimViaWa untuk mengirim data pada tabel proses_antrian ke Whatsapp

        $sendwa = "";

        $status_proses_antrian_json = '{
            "data": [
            {
                "status" : "0",
                "message" : "Berhasil dikirim!"
            },
            {
                "status" : "-1",
                "message" : "Gagal dikirim"
            }
            ]}';
        return $status_proses_antrian_json;
    }

    public function show($id_antrian)
    {
        $id_klien = env('ID_KLIEN');
        $jenis_pemanggilan = "pengumuman";
        $status = "Belum Dilayani";
        // $id_pengguna = auth()->guard('pengguna')->user()->id_pengguna;
        $get_date = date('Y-m-d');
        $get_time = date('h:i:s');

        //get data antrian pengunjung
        $get_data_antrian1 = DB::table('antrian')->select('id_antrian', 'kode_layanan', 'no_antrian', 'nama_pengunjung', 'no_telp_pengunjung')->where('id_antrian', $id_antrian)->first();
        // dd($get_data_antrian1);
        //get data antrian terakhir dengan status belum dilayani
        // $antrian_saat_ini = DB::select("SELECT antrian.kode_layanan, antrian.no_antrian, antrian.id_antrian FROM antrian JOIN proses_antrian ON antrian.id_antrian = proses_antrian.id_antrian WHERE proses_antrian.status = 'Belum Dilayani' AND antrian.kode_layanan = '$get_data_antrian1->kode_layanan' ORDER BY antrian.id_antrian ASC LIMIT 1");
        $antrian_saat_ini = DB::select("SELECT antrian.kode_layanan, antrian.no_antrian, antrian.id_antrian FROM antrian JOIN proses_antrian ON antrian.id_antrian = proses_antrian.id_antrian WHERE proses_antrian.status = 'Belum Dilayani' AND antrian.kode_layanan = '$get_data_antrian1->kode_layanan' AND proses_antrian.tanggal = '$get_date' ORDER BY antrian.id_antrian ASC LIMIT 1");
        // dd($antrian_saat_ini);
        $antrian_saat_ini = (count($antrian_saat_ini) > 0) ? $antrian_saat_ini[0] : (object)['kode_layanan' => '-', 'no_antrian' => '-'];

        return view('publik.proses_antrian', compact('get_data_antrian1', 'antrian_saat_ini'));
    }

    public function cetak_pdf()
    {
    }
}
