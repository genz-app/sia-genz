<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
// use Illuminate\Contracts\Auth\UserProvider;

class AuthLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['logout']]);
        $this->middleware('auth', ['only' => ['logout']]);
    }
    
    public function login()
    {
        return view('/login');
    }

    public function login_pengguna()
    {
        return view('/pengguna/login_pengguna');
    }

    public function lupa_password()
    {
        return view('/lupa_password');
    }

    public function proseslogin(Request $request)
    {
        // $user = DB::table('klien')->where('email', '=', $request->email)->where('password', '=', $request->password)->first();

        // if(Auth::guard('klien')->attempt(['e']))


        // if ($user != null) {
        //     return redirect()->intended('/admin/index');
        // } else {
        //     return back()->withErrors(['email' => 'Email atau password salah.'])->withInput();
        // }

        $validator = Validator::make($request->all(), [
            //request sesuai name inputan
            'email'     => 'required',
            'password'  => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'    => 'error',
                'errors'    => $validator->errors()
            ]);
        }

        $credentials = [
            'email'         => $request->email,
            'password'      => $request->password
        ];

        if (Auth::guard('klien')->attempt($credentials)) {

            $request->session()->regenerate();

            $session = [
                'id'        => Auth()->guard('klien')->user()->id_klien,
                // 'id'        => Auth::guard('klien')->id_klien,
                // 'nama'      => Auth::guard('klien')->nama_klien,
                // 'header'      => Auth::guard('klien')->header_klien,
                // 'footer'      => Auth::guard('klien')->footer_klien,
            ];
            Session::put($session);

            $redirect = route('home-admin');
            // dd(Auth::guard('klien'));
            return response()->json([
                'status'    => 'success',
                'toast'     => 'Login berhasil',
                'resets'    => 'all',
                'redirect'  => $redirect
            ]);
        } else {
            return response()->json([
                'status'    => 'error',
                'toast'     => 'Username atau password salah!',
                'resets'    => 'all'
            ]);
        }

        return response()->json([
            'status'    => 'error',
            'toast'     => 'Login gagal'
        ]);
    }


    public function proseslogin_pengguna(Request $request)
    {
        // $user = DB::table('pengguna')->where('email', '=', $request->email)->where('password_pengguna', '=', $request->password_pengguna)->first();

        // if ($user != null) {
        //     return redirect()->intended('petugas/pemanggilan');
        // } else {
        //     return back()->withErrors(['email' => 'Email atau password salah.'])->withInput();
        // }

        $validator = Validator::make($request->all(), [
            //request sesuai name inputan
            'email'     => 'required',
            'password'  => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'    => 'error',
                'errors'    => $validator->errors()
            ]);
        }

        $credentials = [
            'email'         => $request->email,
            'password'     => $request->password
        ];
        // dd($credentials);

        if (Auth::guard('pengguna')->attempt($credentials)) {

            $request->session()->regenerate();

            $session = [
                'id'        => Auth()->guard('pengguna')->user()->id_pengguna,
                'nama'      => Auth()->guard('pengguna')->user()->nama,
            ];
            Session::put($session);

            $redirect = route('pemanggil');

            return response()->json([
                'status'    => 'success',
                'toast'     => 'Login berhasil',
                'resets'    => 'all',
                'redirect'  => $redirect
            ]);
        } else {
            return response()->json([
                'status'    => 'error',
                'toast'     => 'Username atau password salah!',
                'resets'    => 'all'
            ]);
        }

        return response()->json([
            'status'    => 'error',
            'toast'     => 'Login gagal'
        ]);
    }

    public function logout(Request $request)
    {
        // dd(Auth::guard('klien'));
        // Auth::guard('klien')->logout();
        // Auth::guard('pengguna')->logout();
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect(route('/'));
    }
}
