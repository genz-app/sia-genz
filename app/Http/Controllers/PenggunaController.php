<?php

namespace App\Http\Controllers;

use App\Models\KlienLayanan;
use App\Models\Pengguna;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class PenggunaController extends Controller
{
    public function index(Request $request)
    {
        return view('admin/index');
    }

    public function data()
    {
        $id= env('ID_KLIEN');
        // if (request()->ajax()) {
        // $datapengguna = DB::table('pengguna')->select('id_pengguna',  'nama', 'email', 'no_telp', 'daftar_layanan')->get()->toArray();
        $datapengguna = DB::table('pengguna')
            ->select('id_pengguna',  'nama', 'email', 'no_telp')
            ->where('id_klien', $id)
            ->get();

        // dd($datapengguna);
        return DataTables::of($datapengguna)
            ->make(true);
        // }
    }

    public function dataLayanan(Request $request)
    {
        $id_pengguna = $request->id_pengguna;
        $dataLayanan = DB::table('klien_layanan')
            ->where('id_pengguna', '=', $id_pengguna)
            ->get();

        // dd($dataLayanan);
        return DataTables::of($dataLayanan)->make(true);
    }

    public function insert()
    {
        return view('admin/tambah_pengguna');
    }

    public function store(Request $request)
    {
        $id = env('ID_KLIEN');
        // $daftarlayanan = json_encode($request->daftar_layanan);
        $request->validate([
            'nama' => 'required',
            'email' => 'required|email',
            'password_pengguna' => 'required',
            'no_telp' => ['required', 'max:13'],
        ], [
            'nama.required' => 'Nama Wajib Diisi!',
            'email.required' => 'Email Wajib Diisi!',
            'email.email' => 'Gunakan Format Email Yang Benar!',
            'password_pengguna.required' => 'Password Wajib Diisi!',
            'no_telp.required' => 'Nomor Wajib Diisi!',
        ]);

        DB::beginTransaction();

        try {

            $data = [
                'id_klien' => $id,
                'nama' => $request->nama,
                'email' => $request->email,
                'password' => Hash::make($request->password_pengguna),
                'no_telp' => $request->no_telp,
            ];

            DB::table('pengguna')->insert($data);

            DB::commit();
            return response()->json([
                'status'    => 'success',
                'toast'     => 'Data berhasil disimpan'
            ]);
        } catch (\Exception $e) {
            // dd($e);
            dd($e->getMessage());
            DB::rollback(); // something went wrong
            return response()->json([
                'status'    => 'error',
                'toast'     => 'Data gagal disimpan'
            ]);
        }

        // $datainput = DB::select("select * from public.pengguna_insert('$id', '$request->nama', '$request->email', '$request->password_pengguna', '$request->no_telp', '', '$daftarlayanan')");

        // if (!$datainput) {
        //     return response([
        //         'status' => -1,
        //         'message' => 'Gagal Menambahkan Pengguna!'
        //     ]);
        // } else {
        //     return response([
        //         'status' => 0,
        //         'message' => 'Penambahan berhasil!'
        //     ]);
        // }
    }



    // public function edit($idtemporary)
    // {
    //     DB::select("select * from public.pengguna_getone($idtemporary)");
    //     // return response()->json(['result' => $data]);

    // }

    public function update(Request $request, $idtemporary)
    {
        // $id = env('ID_KLIEN');
        // dd($daftarlayanan);
        // $request->validate([
        //     'nama2' => 'required',
        //     'email2' => 'required|email',
        //     'password_pengguna2' => 'required',
        //     'no_telp2' => 'required',
        // ], [
        //     'nama2.required' => 'Nama Wajib Diisi!',
        //     'email2.required' => 'Email Wajib Diisi!',
        //     'email2.email' => 'Gunakan Format Email Yang Benar!',
        //     'password_pengguna2.required' => 'Password Wajib Diisi!',
        //     'no_telp2.required' => 'Nomor Wajib Diisi!',
        // ]);
        // $datainput = DB::table('pengguna')->where('id_pengguna', $idtemporary)->update(['nama' => $request->nama2, 'email' => $request->email2, 'password_pengguna' => $request->password_pengguna2, 'no_telp' => $request->no_telp2, $daftarlayanan]);
        // $datainput = DB::select("UPDATE pengguna SET nama = '$request->nama2', email = '$request->email2', password_pengguna = '$request->password_pengguna2', no_telp = '$request->no_telp2' WHERE id_pengguna = $idtemporary");

        DB::beginTransaction();

        try {

                $nama       = $request->nama2;
                $email      = $request->email2;
                $password   = Hash::make($request->password_pengguna2);
                $no_telp    = $request->no_telp2;

            DB::select("UPDATE pengguna SET nama = '$nama', email = '$email', password = '$password', no_telp = '$no_telp' WHERE id_pengguna = $idtemporary");


            DB::commit();
            return response()->json([
                'status'    => 'success',
                'toast'     => 'Data berhasil disimpan'
            ]);
        } catch (\Exception $e) {
            // dd($e);
            dd($e->getMessage());
            DB::rollback(); // something went wrong
            return response()->json([
                'status'    => 'error',
                'toast'     => 'Data gagal disimpan'
            ]);
        }

        // DB::beginTransaction();

        // try {
        //         $nama = $request->editnama;
        //         $email = $request->edit;
        //         $password = $request->editpassword;
        //         $no_telp = $request->editnotelp;

        //     DB::select("UPDATE pengguna SET id_klien='$id', nama='$nama', email='$email', password_pengguna='$password', daftar_layanan='$daftarlayanan', no_telp='$no_telp' where id_pengguna='$id'");

        //     DB::commit();
        //     return response()->json([
        //         'status'    => 'success',
        //         'toast'     => 'Data berhasil disimpan'
        //     ]);
        // } catch (\Exception $e) {
        //     // dd($e);
        //     dd($e->getMessage());
        //     DB::rollback(); // something went wrong
        //     return response()->json([
        //         'status'    => 'error',
        //         'toast'     => 'Data gagal disimpan'
        //     ]);
        // }
        
        // $datainput = DB::table('pengguna')->where('id_pengguna', $idtemporary)->update(['nama' => $request->nama2, 'email' => $request->email2, 'password_pengguna' => $request->password_pengguna2, 'no_telp' => $request->no_telp2, $daftarlayanan]);
        // $datainput = DB::select("UPDATE pengguna SET nama = $request->nama2, email = $request->email2, password_pengguna = $request->password_pengguna2, no_telp = $request->no_telp2, daftar_layanan = $daftarlayanan WHERE id_pengguna = $idtemporary");
        // if (!$datainput) {
        //     return response([
        //         'status' => -1,
        //         'message' => 'Gagal Menambahkan Pengguna!'
        //     ]);
        // } else {
        //     return response([
        //         'status' => 0,
        //         'message' => 'Penambahan berhasil!'
        //     ]);
        // }
    }

    public function destroy($idtemporary)
    {
        DB::beginTransaction();

        try {
            $proses_input = DB::table('pengguna')->where('id_pengguna', $idtemporary)->delete();;

            DB::commit();
            return response()->json([
                'status'    => 'success',
                'toast'     => 'Data berhasil dihapus'
            ]);
        } catch (\Exception $e) {
            dd($e);
            DB::rollback(); // something went wrong
            return response()->json([
                'status'    => 'error',
                'toast'     => 'Data gagal dihapus'
            ]);
        }
        // DB::select("select * from public.pengguna_delete($idtemporary)");
        // DB::table('pengguna')->where('id_pengguna', $idtemporary)->delete();
        // return response()->json(['message' => 'Data Berhasil Dihapus']);
    }

    // public function getone()
    // {
    //     $id = 1;
    //     $data_pengguna = DB::select("select * from public.pengguna_getone('$id')");
    //     dd($data_pengguna);
    //     return view ('profil', compact('data_pengguna'));
    // }

    public function search(): JsonResponse
    {
        $data = [];

        $data = KlienLayanan::select("nama_layanan", "id_klien_layanan")->get();

        return response()->json($data);
    }


    public function profile()
    {
        $id_pengguna = auth()->guard('pengguna')->user()->id_pengguna;
        // dd($id_pengguna);
        $data_profile = DB::table('pengguna')
            ->select('nama', 'email', 'no_telp')
            ->where('id_pengguna', '=', $id_pengguna)
            ->first();
        return view('pengguna/profile', compact('data_profile'));
    }

    public function updateProfile(Request $request)
    {
        $id_pengguna = auth()->guard('pengguna')->user()->id_pengguna;

        DB::beginTransaction();

        try {
            $nama = $request->nama;
            $email = $request->email;
            $notelp = $request->notelp;
            $proses_input = DB::select("UPDATE pengguna SET nama='$nama', email='$email', no_telp='$notelp' WHERE id_pengguna='$id_pengguna'");

            DB::commit();
            return response()->json([
                'status'    => 'success',
                'toast'     => 'Data berhasil disimpan'
            ]);
        } catch (\Exception $e) {
            dd($e);
            DB::rollback(); // something went wrong
            return response()->json([
                'status'    => 'error',
                'toast'     => 'Data gagal disimpan'
            ]);
        }
    }
}
