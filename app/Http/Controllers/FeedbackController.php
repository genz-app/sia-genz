<?php

namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class FeedbackController extends Controller
{
    public function getAll()
    {

        // $data_feedback_collect = collect($feedback)->first();

        if (request()->ajax()) {
            $feedback = DB::select('SELECT feedback.skor_aplikasi,
            feedback.skor_pelayanan,
            feedback.kritik_saran,
            feedback.tanggal,
            feedback.jam,
            klien_layanan.nama_layanan from feedback join antrian 
            on feedback.id_antrian = antrian.id_antrian 
            join klien_layanan
            on antrian.id_klien_layanan = klien_layanan.id_klien_layanan ');
            return DataTables::of($feedback)

                ->make(true);
        }
        return view('pengguna.feedback');
        // if (count($feedback)) {
        //     $data_feedback = response()->json([
        //         'status' => 0,
        //         'message' => 'Success!',
        //         'data' => $feedback
        //     ]);
        //     // return $data_feedback;
        //     return view('pengguna.feedback', compact('feedback'));
        // } else {
        //     return response()->json([
        //         'status' => -1,
        //         'message' => 'Tidak ditemukan data',
        //         'data' => null
        //     ]);
        // }

        // return view('feedback.index', compact('feedback'));
    }

    public function getOne()
    {

        $get_one = DB::select('SELECT * FROM public.feedback_getone(5)');
        if (count($get_one)) {
            return response()->json([
                'status' => 0,
                'message' => 'Success!',
                'data' => $get_one
            ]);
        } else {
            return response()->json([
                'status' => -1,
                'message' => 'Tidak ditemukan data',
                'data' => null
            ]);
        }
        // return view('feedback.index1', compact('get_one'));
    }

    public function update()
    {
        $update_feedback = DB::select('SELECT * FROM public.feedback_update($id_feedback, $id_klien, $arr_data_feedback)');

        $status_feedback_json = '{
			"data":[
			{
				"status" : 0,
				"message" : "Successfully Updated!"
			},
			{
				"status" : -1,
				"message" : "Error!"
			}
			]}';
        return $status_feedback_json;
    }

    public function delete()
    {
        $delete_feedback = DB::select('SELECT * FROM public.feedback_delete($id_feedback, $id_klien)');

        $status_feedback_json = '{
			"data":[
			{
				"status" : 0,
				"message" : "Successfully Deleted!"
			},
			{
				"status" : -1,
				"message" : "Error!"
			}
			]}';
        return $status_feedback_json;
    }

    public function insert(string $id_antrian)
    {
        return view('publik/feedback', compact('id_antrian'));
    }

    public function input_feedback(Request $request, string $id_antrian)
    {

        DB::beginTransaction();

        try {
            $id_klien = env('ID_KLIEN');
            $tanggal = date('Y-m-d');
            $jam = date("h:i:sa");
            $proses_input = DB::select("SELECT * FROM public.feedback_insert('$id_klien', '$id_antrian', '$request->skoraplikasi', '$request->skorlayanan', '$request->kritiksaran', '$tanggal', '$jam')");

            DB::commit();
            return response()->json([
                'status'    => 'success',
                'toast'     => 'Data berhasil disimpan'
            ]);
        } catch (\Exception $e) {
            // dd($e);
            DB::rollback(); // something went wrong
            return response()->json([
                'status'    => 'error',
                'toast'     => 'Data gagal disimpan'
            ]);
        }
    }

    // public function pengunjung_input_feedback($id_feedback, $id_klien, $id_klien_layanan, $feedback, $skor_aplikasi, $skor_layanan, $kritik_saran, $tanggal)
    // {
    //     $header_klien = "Klink Ya";
    //     $footer_klien = "Kami buka 24 jam :)";

    //     return view('feedback.input_feedback', compact('header_klien', 'footer_klien'));
    //     $status_feedback_json = '{
    // 		"data":[
    // 		{
    // 			"status" : 0,
    // 			"message" : "Successfully Added!"
    // 		},
    // 		{
    // 			"status" : -1,
    // 			"message" : "Error!"
    // 		}
    // 		]}';
    //     return $status_feedback_json;
    // }
}
