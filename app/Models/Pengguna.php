<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Pengguna extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    public $table = "pengguna";
    
    protected $primaryKey = "id_pengguna";
    protected $fillable = [
        "nama",
        "email",
        "password",
        "no_telp",
        "dafar_layanan",
    ];

    protected $hidden = [
        "password",
        "remember_token",
    ];

    protected $casts = [
        "password" => "hashed",
    ];

    // protected $guard = "pengguna";
}
