<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use \Illuminate\Foundation\Auth\User as Authenticatable;

// class Klien extends Model implements Authenticatable
class Klien extends Authenticatable
{
    use HasFactory;

    public $table = "klien";
    
    protected $primaryKey = "id_klien";
    protected $fillable = [
        "nama_klien",
        "alamat",
        "email",
        "header_klien",
        "footer_klien",
        "password",
    ];

    protected $casts = [
        "id_klien" => "string",
    ];
    
    protected $hidden = [
        "password",
    ];

    // protected $guard = "klien";
}
