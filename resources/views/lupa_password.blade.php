<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
  <meta name="viewport" content="initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/klinikya.css') }}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins">
  <title>Lupa Password</title>
</head>
<body>
    <div class="w-360 mx-auto  lupa-password-page">
      <div class="bg-white form">
        <div class="lupa-password">
          <div class="lupa-password-header">
            <h2>Reset Password</h2>
          </div>
        </div>
        <form class="lupa-password-form">
          <input type="email" placeholder="email"/>
          <input type="password" placeholder="password"/>
          <input type="password" placeholder="konfirmasi password"/>
          <button class="btn btnkirim" type="submit">Reset Password</button>
        </form>
      </div>
    </div>
    <div class="container-fluid footer pt-1 pb-1 bg-white">
        <div class="d-flex justify-content-center">
            <div class="m-0.5 text-center text-sm text-black sm:text-right sm:ml-0">
                Laravel v{{ Illuminate\Foundation\Application::VERSION }} (PHP v{{ PHP_VERSION }})
            </div>
        </div>
    </div>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
</body>
</html>

<style>
    @import url('https://fonts.googleapis.com/css?family=Montserrat:400,800');
  
body{
	background-color: #7D3BEC;
}

.lupa-password-page {
  padding: 8% 0 0;
}

.form {
  position: relative;
  max-width: 360px;
  margin: 0 auto 100px;
  padding: 45px;
  text-align: center;
  box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
}

.form input {
  outline: 0;
  background: #f2f2f2;
  width: 100%;
  border: 0;
  margin: 0 0 15px;
  padding: 15px;
  box-sizing: border-box;
  font-size: 14px;
}

.form button {
  outline: 0;
  width: 100%;
  border: 0;
  color: #FFFFFF;
  font-size: 16px;
  cursor: pointer;
}

</style>