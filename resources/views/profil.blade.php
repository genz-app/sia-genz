@extends ('admin/layout/dashboard')
@section('section')
    <div class="container">
        <div class="main-body">
            <div class="row gutters-sm">
                <div class="col-md-4 mb-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex flex-column align-items-center text-center">
                                <img class="profile" type='file' id="imageUpload" accept=".png, .jpg, .jpeg"
                                    src="{{ asset('assets/image/person1.jpg') }}"
                                    style="width: 240px; height: 240px; border-radius: 50%;">
                                <div class="mt-2">
                                    @foreach ($dataklien as $a)
                                        <h2 class="fw-bold" style="font-size:30px">{{ $a->nama_klien }}</h2>
                                    @endforeach
                                    <br>
                                    <div class="d-grid m-2 gap-2 d-md-flex justify-content-md-center">
                                        <input @change="updatePreview($refs)" x-ref="input" type="file"
                                            accept="image/*,capture=camera" name="photo" id="photo" class="custom">
                                        <label for="photo"
                                            class="btn py-2 px-3 border rounded-2 text-sm leading-4 font-medium bg-primary text-white">
                                            Upload Photo
                                        </label>
                                    </div>
                                    <div class="d-grid m-2 gap-2 d-md-flex justify-content-md-center">
                                        <button type="file" class="btn btn-primary btn-custom">
                                            Reset
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card mb-3">
                        <div class="card-body">
                            <div style="margin: 0,5px 0">
                                <h2><b>Profil</b></h2>
                            </div>
                            <form class="xform" method="POST" action="{{ route('update-profil-klien') }}">
                                @csrf
                                @foreach ($dataklien as $a)
                                    <label style="margin: 15px 0 0 0">Nama</label>
                                    <input type="text" class="form-control border border-dark" name="nama"
                                        value="{{ $a->nama_klien }}" />
                                    <label style="margin: 15px 0 0 0">Email</label>
                                    <input type="email" class="form-control border border-dark" name="email"
                                        value="{{ $a->email }}" />
                                    <label style="margin: 15px 0 0 0">Alamat</label>
                                    <input type="text" class="form-control border border-dark" name="alamat"
                                        value="{{ $a->alamat }}">
                                @endforeach
                                <button type="file" class="btn btn-success" style="margin: 24px 0 0 0">
                                    Simpan
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<style>
    @import url('https://fonts.googleapis.com/css?family=Montserrat:400,800');

    input[type="file"].custom {
        border: 0;
        clip: rect(0, 0, 0, 0);
        height: 1px;
        overflow: hidden;
        padding: 0;
        position: absolute !important;
        white-space: nowrap;
        width: 1px;
    }

    body {
        color: #1a202c;
        text-align: left;
        background-color: #e2e8f0;
    }

    .main-body {
        padding: 15px;
    }

    .card {
        box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06);
    }

    .card {
        position: relative;
        display: flex;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 0 solid rgba(0, 0, 0, 0.125);
        border-radius: 0.25rem;
    }

    .card-body {
        flex: 1 1 auto;
        min-height: 1px;
        padding: 1rem;
    }
</style>
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
                $('#imagePreview').hide();
                $('#imagePreview').fadeIn(650);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#imageUpload").change(function() {
        readURL(this);
    });
</script>
@endsection