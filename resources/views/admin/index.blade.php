@extends('admin/layout/dashboard')

@section('section')
<div class="container-fluid">
    <!-- Tombol Modal tambah -->
    <button type="button" class="btn btn-primary my-3" id="buttontambah" data-bs-toggle="modal">
        + Tambah Petugas
    </button>
    <div class="table-responsive">
        <table class="display" id="tabelPengguna" style="width: 100%;">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>No Telepon</th>
                    <th>Aksi</th>
                </tr>
                <br>
            </thead>
        </table>
    </div>
</div>

<!-- Modal Tambah-->
<div class="modal fade" id="tambahpengguna" tabindex="-1" aria-labelledby="tambahpenggunalabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('insert-pengguna') }}" method="post" id="formtambah" class="xform">
                @csrf
                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="modalheader">DAFTAR PENGGUNA</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="nama" class="form-label">Nama</label>
                        <input type="text" class="form-ctrl" id="nama" name="nama" placeholder="nama" required>
                        @error('nama')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" class="form-ctrl" id="email" name="email" placeholder="contoh@email.com" required>
                        @error('email')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label for="password_pengguna" class="form-label">Password</label>
                        <input type="password" class="form-ctrl" id="password_pengguna" name="password_pengguna" placeholder="password" required>
                        @error('password_pengguna')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label for="no_telp" class="form-label">Nomor Telepon</label>
                        <input type="number" class="form-ctrl" id="no_telp" name="no_telp" placeholder="628xxxxxxxxxx" required>
                        @error('no_telp')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-success" type="submit">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Detail-->
<div class="modal fade" id="detailPengguna" tabindex="-1" aria-labelledby="tambahpenggunalabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="modalheader">DAFTAR LAYANAN<span class="badge bg-info ms-2" id="namaPetugas"></span></h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="card">
                    <div class="card-body table-responsive">
                        <table class="display" id="tableDaftarLayanan" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th class="text-center">Layanan</th>
                                    <th class="text-center">Kode Layanan</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-light" data-bs-dismiss="modal">Tutup</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Edit-->
<div class="modal fade" id="editpengguna" tabindex="-1" aria-labelledby="editpenggunalabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="modalheader">EDIT PENGGUNA</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="" method="post" id="formedit" class="xform">
                @csrf
                <div class="modal-body">
                    <input type="hidden" name="id_pengguna2" id="id_pengguna2">
                    <div class="mb-3">
                        <label for="nama2" class="form-label">Nama</label>
                        <input type="text" class="form-ctrl" id="nama2" name="nama2" placeholder="nama" required>
                        @error('nama')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label for="email2" class="form-label">Email</label>
                        <input type="email" class="form-ctrl" id="email2" name="email2" placeholder="contoh@email.com" required>
                        @error('email')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label for="password_pengguna" class="form-label">Password</label>
                        <input type="password" class="form-ctrl" id="password_pengguna2" name="password_pengguna2" placeholder="password" required>
                        @error('password_pengguna')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label for="no_telp2" class="form-label">Nomor Telepon</label>
                        <input type="number" class="form-ctrl" id="no_telp2" name="no_telp2" placeholder="628xxxxxxxxxx" required>
                        @error('no_telp')
                        <span class="invalid-feedback">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success" type="submit" >Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Konfirmasi Hapus -->
<div class="modal fade" id="hapuspengguna" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Apakah anda yakin menghapus pengguna ini?</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="" method="post" id="formhapus">
                <div class="modal-body">
                    <input class="form-ctrl" type="hidden" id="id_pengguna3" name="id_pengguna3"></input>
                    <div class="mb-3">
                        <label for="nama3" class="form-label"></label><br>
                        <input class="form-ctrl" id="nama3" name="nama3" disabled></input>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Kembali</button>
                    <button type="submit" class="btn btn-danger">Hapus</button>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
    // Datatables
    let datatable;
    let tableLayanan;
    $(document).ready(function() {
        if (undefined !== datatable) {
            datatable.destroy()
            datatable.clear().draw()
        }

        datatable = $('#tabelPengguna').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin-data') }}",
            drawCallback: function(s) {
                $('#tabelPengguna').on('click', '#btnDetail', function(e) {
                    e.preventDefault()

                    let data = datatable.row($(this).parents('tr')).data()

                    detail(data)
                });
                $('#tabelPengguna').on('click', '#buttonedit', function(e) {
                    e.preventDefault()

                    let data = datatable.row($(this).parents('tr')).data()

                    ubah(data)
                });
                $('#tabelPengguna').on('click', '#buttondelete', function(e) {
                    e.preventDefault()

                    var data = datatable.row($(this).parents('tr')).data()

                    hapus(data)
                });

            },
            columns: [{
                    width: '5%',
                    class: 'text-center',
                    render: function(data, type, row, meta) {
                        // Render nomor urut (indeks + 1)
                        return meta.row + 1;
                    },
                    orderable: false
                },
                {
                    width: '30%',
                    data: 'nama'

                },
                {
                    width: '20%',
                    data: 'email'

                },
                {
                    width: '20%',
                    data: 'no_telp'

                },
                {
                    width: '25%',
                    class: 'text-center',
                    "render": function(data, type, row) {
                        return `<a href="#" id="btnDetail" class="btn btn-info btn-sm"><i class="fas fa-list"></i> Detail</a>
                            <a href="#" id="buttonedit" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i> Edit</a>
                            <a href="#" id="buttondelete" class="btn btn-danger btn-sm buttondelete"><i class="fas fa-trash-alt"></i> Hapus</a>`
                    }
                },
            ]
        });
    });


    //Tombol Tambah pengguna
    $('button#buttontambah').on('click', function(e) {
        e.preventDefault()
        $('#tambahpengguna').modal('show');
        $('#formtambah').trigger('reset');
    });

    $('form#formtambah').off('xform-success').on('xform-success', function() {
        datatable.ajax.reload(null, false)
        $('div#tambahpengguna').modal('hide')
    });

    detail = function(id) {
        $('#detailPengguna').modal('show')
        $('#namaPetugas').html(id.nama)

        if (undefined !== tableLayanan) {
            tableLayanan.destroy()
            tableLayanan.clear().draw()
        }


        tableLayanan = $('#tableDaftarLayanan').DataTable({
            processing: true,
            serverSide: true,
            dataType: "json",
            ajax: {
                url: "{{ route('data-layanan-pengguna') }}",
                data: function(data) {
                    data.id_pengguna = id
                }
            },
            columns: [{
                    width: '10%',
                    class: 'text-center',
                    render: function(data, type, row, meta) {
                        // Render nomor urut (indeks + 1)
                        return meta.row + 1;
                    },
                    orderable: false
                },
                {
                    width: '70%',
                    data: 'nama_layanan'

                },
                {
                    width: '20%',
                    class: 'text-center',
                    data: 'kode_layanan'

                }

            ]
        });
    }

    // Proses Edit
    ubah = function(data) {
        $('#id_pengguna2').val(data.id_pengguna)
        $('#nama2').val(data.nama)
        $('#email2').val(data.email)
        // $('#password_pengguna2').val(data.password)
        $('#no_telp2').val(data.no_telp)
        $('#editpengguna').modal('show')

        let id = data.id_pengguna;
        let form = $('#formedit');
        let url = '/admin/pengguna/update/';
        form.attr('action', url + id)

        $('form#formedit').off('xform-success').on('xform-success', function() {
            datatable.ajax.reload(null, false)
            $('div#editpengguna').modal('hide')
        });
    }

    // // Proses delete
    hapus = function(data) {
        $('#id_pengguna3').val(data.id_pengguna)
        $('#nama3').val(data.nama)
        $('#hapuspengguna').modal('show')
        $('#formhapus').submit(function(e) {
            e.preventDefault();

            var id = $('#id_pengguna3').val();
            var formData = $(this).serialize();

            $.ajax({
                type: 'DELETE',
                url: '/admin/pengguna/delete/' + id,
                data: {
                    _token: "{{ csrf_token() }}"
                },
                success: function(result) {
                    if (result.status == 'success') {
                        $('#hapuspengguna').modal('hide')
                        toastr.success(result.toast)
                        datatable.ajax.reload(null, false)
                    } else {
                        toastr.error(result.toast)
                    }
                },
                error: function(err) {
                    if (err.responseJSON) {
                        toastr.error(err.statusText + ' | ' + err.responseJSON.message)
                    } else {
                        toastr.error(err.statusText)
                    }
                }
            });
        });

    }
</script>
@endpush