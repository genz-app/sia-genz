@extends ('admin/layout/dashboard')
@section('section')
    <div class="container-fluid">

        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary my-3" data-bs-toggle="modal" id="tambah">
            + Tambah Layanan
        </button>

        <!-- Modal insert -->
        <div class="modal fade" id="Modalinsert" tabindex="-1" aria-labelledby="tambah layanan" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div>
                        <h5 class="judul-form" id="Modalinsert"><Strong>Tambah Layanan</Strong></h5>

                    </div>
                    <div class="modal-body">
                        <form class="xform" action="/admin/layanan_admin/insert" id="formtambah" method="post">
                            @csrf

                            <div>
                                <label for="Modalinsert" class="form-lbl" required="required">Nama Layanan</label>
                                <br>
                                @error('nama_layanan')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                                <input type="text" class="form-ctrl" id="nama_layanan" name="nama_layanan"
                                    placeholder="Nama Layanan">
                            </div>
                            <br>
                            <div>
                                <label for="Modalinsert" class="form-lbl" required="required">Kode Layanan</label>
                                <br>
                                @error('kode_layanan')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                                <input type="text" class="form-ctrl" id="kode_layanan" name="kode_layanan"
                                    placeholder="Kode Layanan">
                            </div>
                            <br>
                            <div>
                            <label for="Modalinsert" class="form-lbl" required="required">Nama Petugas</label>
                    
                                <select class="form-ctrl" name="petugas">
                                    @foreach ($petugas as $petugas_data)
                                        <option value="{{ $petugas_data->id_pengguna }}">{{ $petugas_data->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <br>
                            <div class="d-grid gap-2 d-md-block">
                                <button type="button" class="btn-cobo" data-bs-dismiss="modal"><strong>
                                        Kembali</strong></button>
                                <button class="btn btn-success" type="submit" id="buttonsimpan">Kirim</button>
                            </div>
                    </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous">
    </script>

    <div class="container-fluid">
        <table class="display" id="tabel-klien-layanan" style="width: 100%;">
            <thead>
                <tr>
                    <th>No Urut</th>
                    <th>Kode Layanan</th>
                    <th>Nama Layanan</th>
                    <th>Aksi</th>
                </tr>
            </thead>

            <tbody>
                <!-- modal update -->
                <div class="modal fade" id="Modalupdate" tabindex="-1" aria-labelledby="Edit Layanan" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div>
                                <h5 class="judul-form" id="Modalupdate"><Strong>Edit Layanan</Strong></h5>
                            </div>
                            <div class="modal-body">
                                <form id="editForm" class="xform" method="POST">
                                    @csrf
                                    <input type="hidden" name="id_klien_layanan" id="id_klien_layanan"
                                        value="id_klien_layanan">
                                    <label>Nama Layanan</label>
                                    <input type="text" name="nama_layanan" id="edit_nama_layanan" class="form-control"
                                        placeholder="nama" required>
                                    <input type="hidden" name="id_klien_layanan" id="id_klien_layanan"
                                        value="id_klien_layanan">
                                    <label>Nama Layanan</label>
                                    <input type="text" name="kode_layanan" id="edit_kode_layanan" class="form-control"
                                        placeholder="kode" required>
                                    <div class="d-grid gap-2 d-md-block">
                                        <button type="button" class="btn-cobo" data-bs-dismiss="modal"><strong>
                                                Kembali</strong></button>
                                        <button class="btn btn-success" type="submit">Kirim</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal Hapus -->
                <div class="modal fade" id="Modaldelete" tabindex="-1" aria-labelledby="Hapus Layanan" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div>
                                <h5 class="judul-form"><Strong>Apakah anda yakin menghapus layanan ini?</Strong></h5>
                            </div>
                            <div class="modal-body">
                                <form id="deleteForm" method="POST">
                                    @csrf
                                    <input type="hidden" name="id_klien_layanan" id="id_klien_layanan1"
                                        value="id_klien_layanan">
                                    <label>Nama Layanan</label>
                                    <input type="text" name="nama_layanan" id="nama_layanan1" class="form-control"
                                        disabled>
                                    <br>
                                    <div class="d-grid gap-2 d-md-block">
                                        <button type="button" class="btn-cobo" data-bs-dismiss="modal"><strong>
                                                Kembali</strong></button>
                                        <button class="btn btn-danger" type="submit">Hapus</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </tbody>
        </table>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        let datatable;

        $(document).ready(function() {
            if (undefined !== datatable) {
                datatable.destroy()
                datatable.clear().draw()
            }

            datatable = $('#tabel-klien-layanan').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('data-layanan-admin') }}",
                drawCallback: function(s) {
                    $('#tabel-klien-layanan').on('click', '#edit', function(e) {
                        e.preventDefault()

                        let data = datatable.row($(this).parents('tr')).data()

                        ubah(data)
                    })
                    $('#tabel-klien-layanan').on('click', '#hapus', function(e) {
                        e.preventDefault()

                        var data = datatable.row($(this).parents('tr')).data()

                        hapus(data)
                    })

                },
                columns: [{
                        class: 'text-center',
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                    },
                    {
                        data: 'kode_layanan',
                        name: 'kode_layanan'
                    },
                    {
                        data: 'nama_layanan',
                        name: 'nama_layanan'
                    },
                    {
                        width: '30%',
                        "render": function(data, type, row) {
                            // return '<a href="" class="btn btn-danger btn-sm" data-bs-toggle="modal" data-bs-target="#ModalHapus"> <i class="fa-regular fa-trash-can"></i> </a> <a href="" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#Modalupdate"> <i class="fas fa-edit"></i> </a>'

                            return `<a href="" class="btn btn-primary btn-sm" id="edit"> <i class="fas fa-edit"></i> </a></a> <a href="" class="btn btn-danger btn-sm" id="hapus">  <i class="fa-regular fa-trash-can"></i> </a>`
                        }
                    }
                ]
            });
        });

        //Setup untuk CSRF token
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('button#tambah').on('click', function(e) {
            e.preventDefault()
            $('#Modalinsert').modal('show');
        })

        $('form#formtambah').off('xform-success').on('xform-success', function() {
            datatable.ajax.reload(null, false)
            $('div#Modalinsert').modal('hide')
        });

        ubah = function(data) {
            $('#id_klien_layanan').val(data.id_klien_layanan)
            $('input#edit_nama_layanan').val(data.nama_layanan)
            $('input#edit_kode_layanan').val(data.kode_layanan)
            $('#Modalupdate').modal('show')

            let id = data.id_klien_layanan;
            let form = $('#editForm');
            let url = '/layanan_admin/update/';
            form.attr('action', url + id)

            $('form#editForm').off('xform-success').on('xform-success', function() {
                datatable.ajax.reload(null, false)
                $('div#Modalupdate').modal('hide')
            });
        }

        hapus = function(data) {
            Swal.fire({
                title: "Apakah Anda yakin untuk menghapus data?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#5D5FEF",
                cancelButtonColor: "#d33",
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak"
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: data.delete,
                        type: 'POST',
                        data: {
                            _token: "{{ csrf_token() }}"
                        },
                        success: function(result) {
                            
                                toastr.success(result.toast)
                                datatable.ajax.reload(null, false)
                        },
                        error: function(err) {
                            if (err.responseJSON) {
                                toastr.error(err.statusText + ' | ' + err.responseJSON.message)
                            } else {
                                toastr.error(err.statusText)
                            }
                        }
                    })
                }
            })
        }
        $('#deleteForm').submit(function(e) {
            e.preventDefault();

            var dataId1 = $('#id_klien_layanan1').val();
            var formData1 = $(this).serialize();

            // Kirim data pembaruan melalui Ajax
            $.ajax({
                type: 'DELETE',
                url: '/layanan_admin/delete/' + dataId1,
                data: formData1,
                success: function(response) {
                    // Tutup modal
                    $('#Modaldelete').modal('hide');
                    // Tampilkan pesan sukses atau lakukan tindakan lain
                    alert(response.message);
                }
            });
        });
    </script>
@endpush
