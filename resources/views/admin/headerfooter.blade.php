@extends ('admin/layout/dashboard')
@section('section')

<div class="content-dashboard row">
        <div class="mx-2 my-2 p-4 col-12"
            style="background-color: white; border-radius: 10px; box-shadow: 5px 5px 100px #00000022;">

<form class="xform" action="/update-header-footer" method="post">
    @csrf
    <div class="mb-3">
        <label for="header_klien">Teks Header</label>
        <input type="text" name="header_klien" value="{{ $data_header_footer->header_klien }}" class="form-control"
         style="background-color:rgb(244, 244, 244)" required>
    </div>
    <div class="mb-3">
        <label for="footer_klien" >Teks Footer</label>
        <input type="text" name="footer_klien" value="{{ $data_header_footer->footer_klien }}" class="form-control"
         style="background-color:rgb(244, 244, 244)" required>
    </div>
    <button type="submit" class="btn btn-primary">Simpan</button>
</form>
</div>
@endsection
