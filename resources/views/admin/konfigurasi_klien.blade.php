@extends ('admin/layout/dashboard')
@section('section')
    <div class="section container-fluid">
        <h4 class="my-4">Pengaturan</h4>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">
                Otomatis kirim melalui whatsapp
            </label>
        </div>
        <br>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label" for="flexCheckDefault">
                Peringatan antrian
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" checked>
            <label class="form-check-label" for="flexCheckChecked">
                pengaturan 3
            </label>
        </div>
        <input class="btn btn-primary btn-sm" type="submit" value="Simpan">
    </div>
@endsection
