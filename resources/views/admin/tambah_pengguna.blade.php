@extends('admin/layout/dashboard')

@section('section')
<nav class="navbar navbar-light bg-transparent">
    <div class="container-fluid">
        <a type="button" class="btn btn-primary" href="/admin/index">
            Kembali
        </a>
    </div>
</nav>
<div class="container-fluid ">
    <div class="container border border-black p-5">
        <h2 class="fw-bold"> DAFTAR PENGGUNA</h2>
        <form action="/admin/pengguna/store" method="post">
            @csrf
            <div class="mb-3">
                <label for="nama" class="form-label">Nama</label>
                <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama" required>
            </div>
            <div class="mb-3">
                <label for="email" class="form-label">Email</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="contoh@email.com" required>
            </div>
            <div class="mb-3">
                <label for="password" class="form-label">Password</label>
                <input type="password" class="form-control" id="password" name="password" placeholder="password" required>
            </div>
            <div class="mb-3">
                <label for="no_telp" class="form-label">Nomor Telepon</label>
                <input type="text" class="form-control" id="no_telp" name="no_telp" placeholder="08xxxxxxxxx" required>
            </div>
            <div class="mb-3">
                <label for="daftar_layanan" class="form-label">Layanan</label>
                <select class="form-control " id="daftar_layanan" name="daftar_layanan" multiple placeholder="Layanan"> </select>
            </div>
            <div class="d-grid gap-2">
                <button class="btn btn-success" type="submit">Kirim</button>
            </div>
        </form>
    </div>
</div>

@endsection
@push('scripts')
<script type="text/javascript">
    $.ajax({
            url: "{{ route('search') }}",
            success: function(res){
                $("select#daftar_layanan").empty().select2({
                    placeholder: 'Pilih Layanan',
                    allowClear: true,
                    data: $.map(res, function(item){
                        return {
                            id: item.nama_layanan,
                            text: item.nama_layanan
                        }
                    })
                }).off('select2:select').on('select2:select', function(e){

                }).off('select2:unselect').on('select2:unselect', function(){

                }).val(null).trigger('change')
            }
        })
</script>
@endpush