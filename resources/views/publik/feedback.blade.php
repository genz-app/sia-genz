@extends('publik/layout/template')

@section('content')
    <div class="content">
        <div class="heading">
            <h1>Bagikan Tanggapan Anda</h1>
        </div>
        <div class="container">
            <div class="row">
                <div class="col mt-4">
                    <form class="py-2 xform" action="/input_feedback/store/{{ $id_antrian }}" style="text-align:center;"
                        method="POST">
                        @csrf
                        <p style="margin-bottom: -10px;">Seberapa puaskah Anda dengan aplikasi kami?</p>
                        <div class="form-group row">
                            <input type="hidden" name="booking_id">
                            <div class="col d-flex justify-content-center">
                                <div class="rate">
                                    <input type="radio" id="star5" name="skoraplikasi" value="5" />
                                    <label for="star5" title="text">5 stars</label>
                                    <input type="radio" id="star4" name="skoraplikasi" value="4" />
                                    <label for="star4" title="text">4 stars</label>
                                    <input type="radio" id="star3" name="skoraplikasi" value="3" />
                                    <label for="star3" title="text">3 stars</label>
                                    <input type="radio" id="star2" name="skoraplikasi" value="2">
                                    <label for="star2" title="text">2 stars</label>
                                    <input type="radio" id="star1" name="skoraplikasi" value="1" />
                                    <label for="star1" title="text">1 star</label>
                                </div>
                            </div>
                        </div>

                        <p style="margin-bottom: -10px; margin-top: 32px;">Seberapa puaskah Anda dengan layanan kami?
                        </p>
                        <div class="form-group row">
                            <input type="hidden" name="booking_id">
                            <div class="col d-flex justify-content-center">
                                <div class="rate">
                                    <input type="radio" id="star-5" class="rate1" name="skorlayanan" value="5" />
                                    <label for="star-5" title="text">5 stars</label>
                                    <input type="radio" id="star-4" class="rate1" name="skorlayanan" value="4" />
                                    <label for="star-4" title="text">4 stars</label>
                                    <input type="radio" id="star-3" class="rate1" name="skorlayanan" value="3" />
                                    <label for="star-3" title="text">3 stars</label>
                                    <input type="radio" id="star-2" class="rate1" name="skorlayanan" value="2">
                                    <label for="star-2" title="text">2 stars</label>
                                    <input type="radio" id="star-1" class="rate1" name="skorlayanan" value="1" />
                                    <label for="star-1" title="text">1 star</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row mt-4">
                            <div class="col">
                                <textarea class="form-control" name="kritiksaran" rows="6 " placeholder="Kritik & Saran" maxlength="200" required></textarea>
                            </div>
                        </div>
                        <div class="d-grid gap-2 mt-4">
                            <!-- <button type="submit" class="btn btn-primer">Kirim</button> -->
                            <button class="btn btn-lg" type="submit" style="background-color: #1964D5; color:white;">Kirim</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
        <script>
            document.addEventListener('DOMContentLoaded', function () {
                const form = document.querySelector('.xform');
                form.addEventListener('submit', function (event) {
                    // Mencegah perilaku pengiriman formulir default
                    event.preventDefault();
                    
                    // Anda dapat menambahkan logika atau validasi tambahan di sini jika diperlukan
                    
                    // Mengarahkan pengguna ke halaman utama
                    window.location.href = '/'; // Ganti '/' dengan URL yang diinginkan
                });
            });
        </script>
@endpush