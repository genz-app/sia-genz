@extends ('publik/layout/template')

@section('content')
    <div class="content">
        <h1><strong>PILIH LAYANAN</strong></h1>
        @foreach ($klien_layanan_nama as $index => $poli)
            <div class="d-grid gap-12 col-lg-6 col-12 mx-auto row "><br>
                <a class="button-klien-layanan poli-{{ $index + 1 }}" href="layanan/input/{{ $poli->id_klien_layanan }}"
                    style="text-decoration: none;">{{ $poli->nama_layanan }}</a>
            </div>
        @endforeach
    </div>
@endsection
