@extends ('publik/layout/template')
@section('content')
    <div class="content">
        <div class="konfirmasi">
            <h1>Pendaftaran Antrian Berhasil</h1>

            <form action="/nomor_antrian/{{ $get_data_antrian->id_antrian }}" method="post">
                @csrf
                <table class="table d-flex justify-content-center mt-5">
                    <tr>
                        <td>Nama Pengunjung</td>
                        <td>
                            <h5>{{ $get_data_antrian->nama_pengunjung }}</h5>
                        </td>
                    </tr>
                    <tr>
                        <td>Nomor Telepon</td>
                        <td>
                            <h5>{{ $get_data_antrian->no_telp_pengunjung }}</h5>
                        </td>
                    </tr>
                    <tr>
                        <td>Nomor Antrian</td>
                        <td>
                            <h5>{{ "$get_data_antrian->kode_layanan - $get_data_antrian->no_antrian" }}</h5>
                        </td>
                    </tr>
                </table>
                <input type="submit" value="Lihat Antrian Saat Ini" class="btn btn-lg mt-4"
                    style="width: 100%; background-color: #1964D5; color: white;">
            </form>


            {{-- <table class="table d-flex justify-content-center mt-5">
                <tr>
                    <td>Nama Pengunjung</td>
                    <td>
                        <h5>{{ $get_data_antrian->nama_pengunjung }}</h5>
                    </td>
                </tr>
                <tr>
                    <td>Nomor Telepon</td>
                    <td>
                        <h5>{{ $get_data_antrian->no_telp_pengunjung }}</h5>
                    </td>
                </tr>
                <tr>
                    <td>Nomor Antrian</td>
                    <td>
                        <h5>{{ "$get_data_antrian->kode_layanan - $get_data_antrian->no_antrian" }}</h5>
                    </td>
                </tr>
            </table>

            <a href="/nomor_antrian/{{ $get_data_antrian->id_antrian }}" class="btn btn-lg mt-4"
                style="width: 100%; background-color: #1964D5; color: white;">Lihat
                antrian
                saat ini<i class="fa-solid fa-right py-2"></i></a> --}}
        </div>
    </div>
@endsection
