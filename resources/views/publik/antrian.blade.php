@extends('publik/layout/template')

@section('content')
    <div class="content">
        <a href="/" class="btn btn-secondary">
            <i class="fa-solid fa-angle-left pe-2"></i>Kembali
        </a>
        <h1><strong>Form Antrian</strong></h1><br/>

        @foreach ($layanan as $data)
            <h1><strong>{{ $data->nama_layanan }}</strong></h1>
            
            <form action="/layanan/input/store/{{ $data->id_klien_layanan }}" method="post">
                @csrf
                <div class="mb-3 mt-4">
                    <label for="exampleFormControlInput1" class="form-label">Nama Pengunjung</label>
                    <input type="text" class="form-control" id="exampleFormControlInput1" name="nama" placeholder="Nama">
                </div>

                <div class="mb-3">
                    <label for="exampleFormControlInput2" class="form-label">Nomor Telepon (+62)</label>
                    <input type="number" class="form-control" id="exampleFormControlInput2" name="nomor_telepon"
                        placeholder="Nomor Telepon" required maxlength="14">
                </div>
                
                <div class="d-grid gap-2 mt-4">
                    <button class="btn btn-lg" type="submit" style="background-color: #1964D5; color:white;">Kirim</button>
                </div>
            </form>
        @endforeach
    </div>
@endsection
