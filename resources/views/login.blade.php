<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous"> -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/klinikya.css') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins">
    <title>Login</title>
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="{{ asset('assets/image/icons/favicon.ico') }}" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/font/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/font/iconic/css/material-design-iconic-font.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/animate/animate.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/css-hamburgers/hamburgers.min.css') }}">
    <!--===============================================================================================-->
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/animsition/css/animsition.min.css') }}"> -->
    <!--===============================================================================================-->
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/select2/select2.min.css') }}"> -->
    <!--===============================================================================================-->
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor/daterangepicker/daterangepicker.css') }}"> -->
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/main.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins">
    <link href="{{ asset('assets/vendor/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/toastr/build/toastr.min.css') }}" rel="stylesheet">

</head>
<style>
    @import url('https://fonts.googleapis.com/css?family=Montserrat:400,800');

    body {
        background-color: #FFFFFF;
    }

    .login-page {
        padding: 8% 0 0;
    }

    .form {
        position: relative;
        max-width: 360px;
        margin: 0 auto 100px;
        padding: 45px;
        text-align: center;
        box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
    }

    .form input {
        outline: 0;
        background: #f2f2f2;
        width: 100%;
        border: 0;
        margin: 0 0 15px;
        padding: 15px;
        box-sizing: border-box;
        font-size: 14px;
        border-radius: 10px;
    }

    .form button {
        outline: 0;
        width: 100%;
        border: 0;
        padding: 5px;
        color: #FFFFFF;
        font-size: 16px;
        cursor: pointer;
    }

    .card {
        border: 0;
    }
</style>

<body>
    <div class="limiter">
        <div class="container-login100" style="background-image: url('{{ asset('assets/image/bg-01.jpg') }}');">
            <div class="wrap-login100">
                <form method="POST" action="/proseslogin" class="login100-form validate-form xform">
                    @csrf
                    <span class="login100-form-title p-b-34 p-t-27">
                        SIA
                    </span>

                    <span class="login100-form-title p-b-34 p-t-27 mb-4">
                        Log in
                    </span>

                    <div class="wrap-input100 validate-input" data-validate="Enter username">
                        <input class="input100" id="email" type="email" placeholder="email" name="email" required autocomplete="email" autofocus />
                        <span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>

                    <div class="wrap-input100 validate-input" data-validate="Enter password">
                        <input class="input100" id="password" type="password" placeholder="password" name="password" required />
                        <span class="focus-input100" data-placeholder="&#xf191;"></span>
                    </div>

                    <div class="contact100-form-checkbox">
                        <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
                        <label class="label-checkbox100" for="ckb1">
                            Remember me
                        </label>
                    </div>

                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn" type="submit">
                            Login
                        </button>
                    </div>
                </form>
                <div class="text-center  mt-3">
                    <a href="{{ route('login_pengguna') }} " class="txt1">Login Sebagai Petugas?</a>
                </div>
            </div>
        </div>
    </div>

    <!--===============================================================================================-->
    <script src="{{ asset('assets/vendor/jquery/jquery-3.2.1.min.js') }}"></script>
    <!--===============================================================================================-->
    <!-- <script src="{{ asset('assets/vendor/animsition/css/animsition.min.css') }}"></script> -->
    <!--===============================================================================================-->
    <script src="{{ asset('assets/vendor/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <!--===============================================================================================-->
    <!-- <script src="{{ asset('assets/vendor/select2/select2.min.css') }}"></script> -->
    <!--===============================================================================================-->
    <!-- <script src="{{ asset('assets/vendor/daterangepicker/moment.min.js') }}"></script> -->
    <!-- <script src="{{ asset('assets/vendor/daterangepicker/daterangepicker.js') }}"></script> -->
    <!--===============================================================================================-->
    <!-- <script src="{{ asset('assets/vendor/countdowntime/countdowntime.js') }}"></script> -->
    <!--===============================================================================================-->
    <script src="{{ asset('assets/js/main.js') }}"></script>
    <script src="{{ asset('assets/js/myfunction.js')}}"></script>

    <script src="{{ asset('assets/vendor/toastr/build/toastr.min.js') }}"></script>
    <script src="{{ asset('assets/js/myfunction.js') }}"></script>
    <script src="{{ asset('assets/vendor/pace-progress/pace.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/sweetalert2/dist/sweetalert2.min.js') }}"></script>

    <!-- CSRF Token -->
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

</body>


</html>