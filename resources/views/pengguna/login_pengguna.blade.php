<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous">
  </script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/klinikya.css') }}">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins">
  <link href="{{ asset('assets/vendor/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/vendor/toastr/build/toastr.min.css') }}" rel="stylesheet">
  <title>Login Pengguna</title>
</head>

<body>
  <div class="row">
    <div class="col-sm-6">
      <div class="card-body">
        <div class="card">
          <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
              <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
              <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
              <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
            </div>
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src="{{ asset('assets/image/antrian 1.png') }}" class="d-block w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="{{ asset('assets/image/antrian 2.png') }}" class="d-block w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="{{ asset('assets/image/antrian 3.jpg') }}" class="d-block w-100" alt="...">
              </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Next</span>
            </button>
          </div>
        </div>
      </div>
    </div>

    <div class="col-sm-6">
      <div class="card">
        <div class="card-body">
          <div class="w-360 mx-auto login-page">
            <div class="bg-white form">
              <div class="login">
                <div class="login-header">
                  <h2 class="fw-bolder fs-3 mt-15">Selamat Datang!</h2>
                  <p class="fs-12">Silahkan login untuk melanjutkan</p>
                </div>
              </div>
              <form method="POST" action="/proseslogin_pengguna" class="xform">
                @csrf
                <input id="email" type="email" placeholder="email" name="email" autocomplete="email" autofocus required />
                <input id="password" type="password" placeholder="password" name="password" required />
                <button class="btn btn-primary" type="submit">Masuk</button>
                <hr>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="{{ asset('assets/vendor/jquery/jquery-3.2.1.min.js') }}"></script>

  <script src="{{ asset('assets/js/myfunction.js')}}"></script>
  <script src="{{ asset('assets/vendor/toastr/build/toastr.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/pace-progress/pace.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/sweetalert2/dist/sweetalert2.min.js') }}"></script>
</body>

</html>
<style>
  @import url('https://fonts.googleapis.com/css?family=Montserrat:400,800');

  body {
    background-color: #FFFFFF;
  }

  .login-page {
    padding: 8% 0 0;
  }

  .form {
    position: relative;
    max-width: 360px;
    margin: 0 auto 100px;
    padding: 45px;
    text-align: center;
    box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
  }

  .form input {
    outline: 0;
    background: #f2f2f2;
    width: 100%;
    border: 0;
    margin: 0 0 15px;
    padding: 15px;
    box-sizing: border-box;
    font-size: 14px;
    border-radius: 10px;
  }

  .form button {
    outline: 0;
    width: 100%;
    border: 0;
    padding: 5px;
    color: #FFFFFF;
    font-size: 16px;
    cursor: pointer;
  }

  .card {
    border: 0;
  }

  a {
    width: 360px;
    margin: auto;
    font-size: 12px;
  }
</style>