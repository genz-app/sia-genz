<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Petugas</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('assets/css/klinikya.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/dashboard/css/style.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" crossorigin="anonymous">
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet">
    <link href="{{ asset('assets/vendor/sweetalert2/dist/sweetalert2.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/vendor/toastr/build/toastr.min.css') }}">
</head>

<body>
    <div class="wrapper d-flex align-items-stretch">
        <nav id="sidebar">
            <div class="p-4 menu-samping">
                <h4><a href="#" class="logo text-white">Petugas</a></h4>
                <hr style="border: 1px solid white;">
                <ul class="list-unstyled components">
                    <li class="{{ request()->is('petugas/pemanggilan') ? 'active-menu active' : '' }}">
                        <a href="/petugas/pemanggilan"><i class="fa-solid fa-list-ol link-sidebar"></i>Pemanggilan</a>
                    </li>
                    <li class="{{ request()->is('petugas/antrian_pengguna') ? 'active-menu active' : '' }}">
                        <a href="/petugas/antrian_pengguna"><i class="fa-solid fa-clipboard-list link-sidebar"></i>Antrian</a>
                    </li>
                    <li class="{{ request()->is('petugas/feedback') ? 'active-menu active' : '' }}">
                        <a href="/petugas/feedback"><i class="fa-solid fa-comments link-sidebar"></i>Feedback</a>
                    </li>
                    <li class="{{ request()->is('/petugas/profile') ? 'active-menu active' : '' }}">
                        <a href="/petugas/profile"><i class="fa-solid fa-user link-sidebar"></i>Profile</a>
                    </li>

                </ul>
                <div class="bottom-menu">
                    <ul class="list-unstyled components">
                        <li>
                            <a href="#" class="logout"><i class="fa-solid fa-right-from-bracket link-sidebar"></i>Logout</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div id="content">
            <nav class="navbar navbar-light" style="background-color: #fff;">
                <div class="container-fluid">
                    <div>
                        <h3>SIA (Sistem Informasi Antrian)</h3>
                    </div>
                    <div class="d-flex">
                        <div class="dropdown">
                            <button class="dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                <img class="profile" src="{{ asset('assets/image/person1.jpg') }}">
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <li><a class="dropdown-item logout" href="#">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <div class="p-3">
                @yield('section')
            </div>
        </div>
    </div>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="{{ asset('assets/js/myfunction.js') }}"></script>
    <script src="{{ asset('assets/vendor/toastr/build/toastr.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/pace-progress/pace.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/sweetalert2/dist/sweetalert2.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $('.logout').on('click', function() {
            Swal.fire({
                title: "Apakah Anda Yakin?",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Keluar",
                cancelButtonText: "Batal",
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = "{{route('logout')}}"
                }
            });
        })
    </script>
    @stack('scripts')
</body>

</html>