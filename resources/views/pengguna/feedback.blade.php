{{-- @extends('pengguna/layout/dashboard')

@section('section')
    <div class="mt-3 bg-white">
        <div class="seacrh-bar row">
            <div class="col-8"></div>
            <div class="col-4">
                <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success" type="submit">Search</button>
            </div>
        </div>
        <div class="tabel-feedback">
            <table class="table">
                <thead>
                    <th class="col-3">Skor Aplikasi</th>
                    <th class="col-3">Skor Layanan</th>
                    <th class="col-6">Kritik & Saran</th>
                </thead>
                @foreach ($feedback as $data)
                    <tr>
                        <td>{{ $data->skor_aplikasi }}</td>
                        <td>{{ $data->skor_pelayanan }}</td>
                        <td>{{ $data->kritik_saran }}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection --}}


@extends('pengguna/layout/dashboard')
@section('section')
    <div class="content-dashboard p-2">
        <table class="display" id="tabel-feedback">
            <thead>
                <tr>
                    <th>Skor Aplikasi</th>
                    <th>Skor Pelayanan</th>
                    <th>Kritik Saran</th>
                    <th>Tanggal</th>
                    <th>Waktu</th>
                    <th>Pelayanan</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous">
    </script>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#tabel-feedback').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ url()->current() }}',
                columns: [{
                        data: 'skor_aplikasi',
                        name: 'skor_aplikasi'
                    },
                    {
                        data: 'skor_pelayanan',
                        name: 'skor_pelayanan'
                    },
                    {
                        data: 'kritik_saran',
                        name: 'kritik_saran'
                    },
                    {
                        data: 'tanggal',
                        name: 'tanggal'
                    },
                    {
                        data: 'jam',
                        name: 'jam'
                    },
                    {
                        data: 'nama_layanan',
                        name: 'nama_layanan'
                    },

                ]
            });
        });
    </script>
@endpush
