@extends('pengguna/layout/dashboard')
@section('section')

<div class="mb-4 row">
    <div class="col-md-3">
        <i class="fa-lg fa-solid fa-filter me-2 text-info-emphasis"></i>
        <label for="input1">Filter Layanan:</label>
        <select class="form-control" id="input1" name="input1" style="height: 40px;">
            <option value="">Semua Layanan</option>
        </select>
    </div>
    <div class="col-md-3">
        <i class="fa-lg fa-solid fa-calendar me-2 text-info-emphasis"></i>
        <label for="tanggal_start">Tanggal Mulai:</label>
        <input type="date" class="form-control" id="tanggal_start" name="tanggal_start" style="color: #999; border: 1px solid #999; height:30px">
    </div>
    <div class="col-md-3">
        <i class="fa-lg fa-solid fa-calendar me-2 text-info-emphasis"></i>
        <label for="tanggal_end">Tanggal Selesai:</label>
        <input type="date" class="form-control" id="tanggal_end" name="tanggal_end"  style="color: #999; border: 1px solid #999; height:30px">
    </div>
    <div class="col-md-3 mt-4">
        <button type="button" id="filter_button" class="btn btn-primary" style="margin-top: 8px; padding: 3px 15px">Filter</button>
    </div>
</div>

<table class="display" id="tabel-antrian">
    <thead> 
        <tr>
            <th scope="col">Nama Layanan</th>          
            <th scope="col">No Antrian</th>
            <th scope="col">Nama</th>
            <th scope="col">No Telp</th>
            <th scope="col">Tanggal</th>
            <th scope="col">Waktu</th>
            <th scope="col">Status</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous">
</script>
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        var tabel;

        tabel = $('#tabel-antrian').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('antrean.index') }}",
                data: function(data) {
                    data.input1 = $('select#input1').val()
                    data.tanggal_start = $('#tanggal_start').val();
                    data.tanggal_end = $('#tanggal_end').val();
                }
            },
            columns: [{
                data: 'nama_layanan',
                name: 'nama_layanan',
                orderable: false,
            }, {
                data: null,
                orderable: false,
                render: function (data, type, row) {
                        var details = row.kode_layanan + " - " + row.no_antrian ;
                        return details;
                    }

            }, {
                data: 'nama_pengunjung',
                name: 'nama_pengunjung'
            }, {
                data: 'no_telp_pengunjung',
                name: 'no_telp_pengunjung'
            }, {
                data: 'tanggal',
                name: 'tanggal'
            },
            {
                data: 'waktu',
                name: 'waktu'
            },{
                data: 'status',
                name: 'status',
                render: function (data, type, row) {
                    var statusColorClass = data === 'Belum Dilayani' ? 'badge text-bg-warning' : data ==='Dilewati' ? 'badge text-bg-danger' : 'badge text-bg-success' ;
                    return '<span class="' + statusColorClass + ' d-inline-block w-100 text-center">' + data + '</span>';
                }
            }]
        });

        // Fungsi untuk mengambil dan mengisi pilihan filter
        function populateFilterOptions() {
            $.ajax({
                url: "{{ route('antrean.select') }}",
                success: function(res) {
                    $("select#input1").empty().select2({
                        placeholder: 'Pilih Layanan',
                        allowClear: true,
                        data: [{
                            id: '',
                            text: 'Semua Layanan'
                        }].concat($.map(res, function(item) {
                            return {
                                id: item.id_klien_layanan,
                                text: item.nama_layanan
                            }
                        }))

                    }).off('select2:select').on('select2:select', function(e) {
                        // Ketika pengguna memilih filter, muat ulang tabel
                        tabel.ajax.reload();
                    }).off('select2:unselect').on('select2:unselect', function() {
                        // Ketika pengguna menghapus filter, muat ulang tabel
                        tabel.ajax.reload(); 
                    }).val(null).trigger('change');
                }
            });
        }

        // Panggil fungsi untuk mengisi pilihan filter
        populateFilterOptions();
        $('#filter_button').click(function() {
            tabel.ajax.reload();
        });
    });
</script>
@endpush
