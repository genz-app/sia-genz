{{-- <meta http-equiv="refresh" content="5"> --}}
@extends('pengguna/layout/dashboard')
@section('section')
    <div class="content-dashboard p-2">
        <a href="/petugas/pemanggilan" class="btn btn-primary"><i class="fa-solid fa-angle-left pe-2"></i>Kembali</a>
        <div class="form-pemanggilan mt-3">
            <h4 style="text-align: center;">Pemanggilan Pengunjung Layanan</h4>
            @foreach ($layanan as $item)
                <h3 style="text-align: center; font-weight:bold">{{ $item->nama_layanan }}</h3>
                <div class="text-center mt-4 py-2">
                    <h5>Antrian Saat Ini:</h5>
                </div>
            @endforeach
            @foreach ($antrian_belum_dilayani as $item1)
                <div class="input-group" style="padding: 0 430px;">

                    {{-- <span class="input-group-text" id="addon-wrapping">{{ $item->kode_layanan }}</span> --}}
                    <input value="{{ $item1->kode_layanan }}" type="text" class="form-control antrian-nomor"
                        style="font-size: 20pt;" name="kode_layanan" aria-label="Username" aria-describedby="addon-wrapping"
                        disabled>
                    <input value="{{ $item1->no_antrian }}" type="number" class="form-control antrian-nomor"
                        style="font-size: 20pt;" name="no_antrian" aria-label="Username" aria-describedby="addon-wrapping"
                        disabled>

                </div>
                <div class="btn-group px-5" role="group" style="width: 100%">
                    <button type="button" class="btn btn-warning btn-lg mt-4" id-antrian="{{ $item1->id_proses_antrian }}"
                        id="btn-antrian-dilewati" style="width: 100%">Lewati Antrian</button>
                    <button type="button" class="btn btn-primary btn-lg mt-4" id-antrian="{{ $item1->id_proses_antrian }}"
                        id="btn-antrian-selanjutnya" style="width: 100%">Antrian Selanjutnya<i
                            class="fa-solid fa-arrow-right ps-2"></i></button>
                </div>
            @endforeach
        </div>
        <div class="row mt-2 mx-1">
            <div class="data-antrian col-6" style="padding: 30px 20px;">
                <h5 style="text-align: center;">Daftar Antrian</h5>
                <table class="table mt-4" style="font-size: 10pt;">
                    <thead>
                        <tr>
                            <th>Nomor Antrian</th>
                            <th>Nama Pengunjung</th>
                            <th>Waktu</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    @foreach ($data_antrian as $antrian)
                        <tbody>
                            <tr>
                                <td>{{ "$antrian->kode_layanan - $antrian->no_antrian" }}</td>
                                <td>{{ $antrian->nama_pengunjung }}</td>
                                <td>{{ $antrian->waktu }}</td>
                                <td><?php
                                if ($antrian->status == 'Selesai') {
                                    echo '<span class="badge bg-success text-light">' . $antrian->status . '</span>';
                                } elseif ($antrian->status == 'Belum Dilayani') {
                                    echo '<span class="badge bg-warning text-dark">' . $antrian->status . '</span>';
                                }
                                ?></td>
                            </tr>
                        </tbody>
                    @endforeach
                </table>
            </div>
            <div class="data-antrian col-6" style="padding: 30px 20px;">
                <h5 style="text-align: center;">Daftar Antrian Dilewati</h5>
                <table class="table mt-4" style="font-size: 10pt;">
                    <thead>
                        <tr>
                            <th>Nomor Antrian</th>
                            <th>Nama Pengunjung</th>
                            <th>Waktu</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    @foreach ($data_antrian_dilewati as $antrian)
                        <tbody>
                            <tr>
                                <td>{{ "$antrian->kode_layanan - $antrian->no_antrian" }}</td>
                                <td>{{ $antrian->nama_pengunjung }}</td>
                                <td>{{ $antrian->waktu }}</td>
                                <td><a href="#" class="btn btn-sm btn-primary text-white">Panggil</a></td>
                            </tr>
                        </tbody>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $('button#btn-antrian-selanjutnya').on('click', function() {
            let id_antrian = $(this).attr('id-antrian')
            $.ajax({
                type: 'GET',
                url: "{{ route('antrian_selanjutnya') }}",
                data: {
                    id_antrian: id_antrian
                },
                success: function(data) {
                    location.reload();
                }
            })
        })

        $('button#btn-antrian-dilewati').on('click', function() {
            let id_antrian = $(this).attr('id-antrian')
            $.ajax({
                type: 'GET',
                url: "{{ route('antrian_dilewati') }}",
                data: {
                    id_antrian: id_antrian
                },
                success: function(data) {
                    location.reload();
                }
            })
        })
    </script>
@endpush
