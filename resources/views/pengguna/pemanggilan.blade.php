@extends('pengguna/layout/dashboard')
@section('section')
<div class="content-dashboard p-2">
    <h5>Pemanggilan Antrian Pengunjung</h5>
    <p>Pilih salah satu layanan</p>
    <div class="tombol-layanan row mt-3">
        @foreach ($klien_layanan_nama as $index => $poli)
        <div class="col-4 ">
            <a class="btn btn-lg button-klien-layanan poli-{{ $index + 1 }}" style="margin: 10px 0" href="/petugas/pemanggilan/{{ $poli->id_klien_layanan }}">{{ $poli->nama_layanan }}</a>
        </div>
        <!-- {{-- <form action="{{ route('pemanggilan.edit', $poli->id_klien_layanan) }}" method="post">
                    <div class="d-grid gap-2 ">
                        <input type="submit" name="layanan" value="{{ $poli->nama_layanan }}"
                            class="btn btn-lg button-klien-layanan poli-{{ $index + 1 }}">
                        <button class="btn btn-lg button-klien-layanan poli-{{ $index + 1 }}"
                            data-bs-target="#modal.antrian" data-bs-toggle="modal" data-bs-dismiss="modal">
                            {{ $poli->nama_layanan }}
                        </button>
                    </div>
                </form> --}} -->
        @endforeach
    </div>
</div>
@endsection
