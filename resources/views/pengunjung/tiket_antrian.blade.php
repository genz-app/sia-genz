@extends ('pengunjung/layout/template')
@section('content')
    <div class="content">
        <div class="konfirmasi">
            <h1>Pendaftaran Antrian Berhasil</h1>
            <table class="table d-flex justify-content-center mt-5">
                <tr>
                    <td>Nama Pengunjung</td>
                    <td>
                        <h5>{{ $get_data_antrian->nama_pengunjung }}</h5>
                    </td>
                </tr>
                <tr>
                    <td>Nomor Telepon</td>
                    <td>
                        <h5>{{ $get_data_antrian->no_telp_pengunjung }}</h5>
                    </td>
                </tr>
                <tr>
                    <td>Nomor Antrian</td>
                    <td>
                        <h5>{{ "$get_data_antrian->kode_layanan - $get_data_antrian->no_antrian" }}</h5>
                    </td>
                </tr>
            </table>
            <a href="/pengunjung/antrian/{{ $get_data_antrian->id_antrian }}" class="btn btn-lg btn-success mt-5"
                style="width: 100%;"><i class="fa-solid fa-print px-2"></i>Cetak Tiket Antrian</a>
            <a href="/pengunjung" class="btn btn-lg btn-secondary mt-2" style="width: 100%;"><i
                    class="fa-solid fa-house px-2"></i>Kembali ke Halaman Utama</a>
        </div>
    </div>
@endsection
