


  //jsku
  const generate_token = function(){
    let result = '';
    let characters = '1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
    let charlength = characters.length;

    for(let i = 0; i < 5; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charlength));
    }

    return result;
}

const form = function(){
    toastr.options = {
		closeButton: true,
		debug: false,
		newestOnTop: true,
		progressBar: true,
		positionClass: 'toast-bottom-right',
		preventDuplicates: false,
		onclick: null,
		showDuration: 300,
		hideDuration: 1000,
		timeOut: 5000,
		extendedTimeOut: 1000,
		showEasing: 'swing',
		hideEasing: 'linear',
		showMethod: 'slideDown',
		hideMethod: 'fadeOut'
	}


    $('form.xform').each(function(){

        let form = $(this);

        form.attr('onsubmit', 'return false')

        form.off('submit').on('submit', function(){
            Pace.start()
            let action = form.attr('action'),
                method = form.attr('method');
                
            var data = new FormData(this)
            
            $.each($(this).data(), function(name, value){
                
                data.append(name, value)
            })

            form.trigger('xform-submit', [data])

            Pace.start()
            $.ajax({
                url : action,
                type : method,
                data: data,
                processData: false,
                contentType: false,
                beforeSend: function () {
					Swal.fire({
						title: 'Data sedang diproses...',
						html: 'Tunggu sebentar...',
						allowEscapeKey: false,
						allowOutsideClick: false,
						didOpen: () => {
							Swal.showLoading()
						}
					});
				},
                success : function(res){
                    Pace.stop()
                    swal.close()

                    let fields = [
                        form.find('input'),
                        form.find('select'),
                        form.find('textarea')
                    ]

                    for(i in fields){

                        fields[i].removeClass('is-invalid')
                        fields[i].parent().find('span.invalid-feedback').remove()
                    }

                    if ('success' == res.status) {

                        form.trigger('xform-success', [res])

                    } else if ('info' == res.status) {

                        form.trigger('xform-info', [res])

                    } else if ('error' == res.status) {

                        form.trigger('xform-error', [res])

                    } else if ('warning' == res.status) {

                        form.trigger('xform-warning', [res])
                    }


                    if (res.resets) {

                        if('all' === res.resets) {

                            form.trigger('reset')
                            form.find('label.custom-file-label').html('Choose file')

                        } else {

                            for(i in res.resets) {

                                let name = res.resets[i]
                                form.find('input[name="'+name+'"]').val('')
                                form.find('select[name="'+name+'"]').val('')
                                form.find('textarea[name="'+name+'"]').html('')
                                form.find('label.custom-file-label').html('')
                            }
                        }
                    }

                    if (res.errors) {

                        focus_first_error_field = true

                        for(field in res.errors) {

                            let message = res.errors[field][0]

                            let fields = [
                                form.find('input[name="'+field+'"]'),
                                form.find('select[name="'+field+'"]'),
                                form.find('textarea[name="'+field+'"]')
                            ]

                            for(i in fields){

                                fields[i].addClass('is-invalid')
                                fields[i].parent().append('<span class="invalid-feedback">' + message + '</span>')

                                if (focus_first_error_field) {
                                    fields[i].focus()
                                    focus_first_error_field = false
                                }
                            }
                        }
                    }

                    if (res.toast) {

                        if ('success' == res.status) {

                            toastr.success(res.toast)
                        } else if ('info' == res.status) {

                            toastr.info(res.toast)
                        } else if ('error' == res.status) {

                            toastr.error(res.toast)
                        } else if ('warning' == res.status) {

                            toastr.warning(res.toast)
                        }
                    } 

                    if (res.redirect) {

                        setTimeout(function(){
                            toastr.info('Mengalihkan...')
                        }, 1000)

                        setTimeout(function(){
                            window.location.href = res.redirect
                        }, 2000)
                    }
                },
                error: function(err){
                    Pace.stop()
                    swal.close()
                    
                    if(err.responseJSON){ph
                        toastr.error(err.statusText + ' | ' + err.responseJSON.message)
                    }else{
                        toastr.error(err.statusText)
                    }
                }
            })
        })
    })
}

const TriggerReset = function(form){
    form.trigger('reset');
        
    let fields = [
        form.find('input'),
        form.find('select'),
        form.find('textarea')
    ]

    for(i in fields){

        fields[i].removeClass('is-invalid')
        fields[i].parent().find('span.invalid-feedback').remove()
    }
}

const xmodal = function(title, content, size = '', tipe = '', footer = ''){

    if (size) {
        // sm, lg, xl, full-width
        size = ' modal-' + size
    }

    if (tipe) {

        tipe = ' bg-' + tipe
    }

    if (footer) {
        footer = '<div class="modal-footer">' + footer + '</div>'
    }

    let
        modal_id = generate_token(),
        modal_label_id = generate_token(),
        modal_el = '    <div class="modal fade" id="' + modal_id + '" tabindex="-1" role="dialog" aria-labelledby="'+
        	modal_label_id+'" aria-hidden="true">'

        modal_el += '       <div class="modal-dialog' + size + '">'
        modal_el += '           <div class="modal-content">'
        modal_el += '               <div class="modal-header d-flex align-items-center modal-colored-header ' + tipe
        	+ '">'

        modal_el += '                   <h4 class="modal-title text-white" id="'+modal_label_id+'">' + title + '</h4>'
        modal_el += '                   <button class="close ml-auto" data-dismiss="modal" aria-hidden="true">x'
        modal_el += '                   </button>'
        modal_el += '               </div>'
        modal_el += '               <div class="modal-body">'
        modal_el += '                   ' + content
        modal_el += '               </div>' + footer
        modal_el += '           </div>'
        modal_el += '       </div>'
        modal_el += '   </div>'

    $('body').append(modal_el)

    let modal = $('div#' + modal_id)

    return modal
}

const confirm = function(
	confirm_action,
	color = 'info',
	title = 'Perhatian',
	message = 'Aksi ini membutuhkan tanggapan, Apakah anda yakin?',
	button_ok_title = 'Lanjutkan',
	button_cancel_title = 'Batalkan'){

	let modal_confirm = xmodal(
        '<i class="ti-info-alt"></i> ' + title,
        '<h4>' + message + '</h4>',
        'lg',
        color,
        '<button class="btn text-white waves-effect waves-light btn-' + color + '"' +
        'id="confirm_action_trigger">' +
        '<i class="ti-check"></i> ' + button_ok_title +
        '</button>' +
        '<button type="button"' +
        'class="btn waves-effect waves-light btn-secondary"' +
        'data-dismiss="modal">' +
        '<i class="ti-close"></i> ' + button_cancel_title +
        '</button>'
    )

    modal_confirm.on('show.bs.modal', function(){

    	modal_confirm.off('hidden.bs.modal')

    	if ($('body').hasClass('modal-open')){
    		modal_confirm.on('hidden.bs.modal', function(){
		        $('body').addClass('modal-open')
		    })
    	}
    })

    modal_confirm.on('hidden.bs.modal', function(){

        modal_confirm.remove()

    }).on('shown.bs.modal', function(){

        $('button#confirm_action_trigger').off('click').click(function(){
        	confirm_action()
            modal_confirm.modal('hide')
        })

    }).modal('show')
}

const currency_id = {
    symbol: '',
    separator: '.',
    decimal: ',',
    precision: 2,
    negativePattern: `(!#)`
}

const decimal_format = function(foo){

	return foo.replace(/\./g, '').replace(/\,/g, '.')
}

const money = function(foo){
	return currency(parseFloat(foo), currency_id).format()
}

const money_obj = function(foo){
	return currency(parseFloat(foo), currency_id)
}

const numberFormat = (number, decimals, dec_point, thousands_sep) => {
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = typeof thousands_sep === 'undefined' ? ',' : thousands_sep,
        dec = typeof dec_point === 'undefined' ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
};


$(function(){
    form()
})