<?php

use App\Http\Controllers\LayananController;
use App\Http\Controllers\AntrianController;
use App\Http\Controllers\FeedbackController;
use App\Http\Controllers\KlienColorController;
use App\Http\Controllers\KonfigurasiKlienController;
use App\Http\Controllers\ProsesAntrianController;
use App\Http\Controllers\AntrianPenggunaController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PenggunaController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LayananAdminController;
use App\Http\Controllers\PemanggilanController;
use App\Http\Controllers\AuthLoginController;
use App\Http\Controllers\HeaderFooterController;
use App\Http\Controllers\PengunjungController;
use App\Http\Controllers\PengaturanController;
use App\Http\Controllers\ProfileController;
use App\Models\klien_layanan;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

/*
Route::prefix('auth')->group( function () {
    Route::get('/login',[AuthLoginController::class,'login'])->name('auth.login');
    Route::post('/logout', [AuthLoginController::class, 'logout'])->name('auth.logout');
});
*/

// Route::get('/', function () {
//     return redirect('login');
// });
Route::get('/', [LayananController::class, 'index'])->name('/');
Route::get('/login', [AuthLoginController::class, 'login'])->name('login');
Route::get('/login_pengguna', function () {
    return view('pengguna.login_pengguna');
})->name('login_pengguna');
Route::post('/proseslogin', [AuthLoginController::class, 'proseslogin']);
Route::post('/proseslogin_pengguna', [AuthLoginController::class, 'proseslogin_pengguna']);
Route::get('/logout', [AuthLoginController::class, 'logout'])->name('logout');

//ROUTE PUBLIK
//route layanan
// Route::get('layanan', [LayananController::class, 'index']);
// Route::get('layanan', [LayananController::class, 'index']);
Route::get('layanan/input/{id_klien_layanan}', [LayananController::class, 'show']);
Route::post('layanan/input/store/{id_klien_layanan}', [LayananController::class, 'update']);
Route::get('antrian/cetak_pdf', [LayananController::class, 'cetak_pdf']);

// route feedback
Route::get('input_feedback/{id_antrian}', [FeedbackController::class, 'insert']);
Route::get('petugas/feedback', [FeedbackController::class, 'getAll']);
Route::get('index1', [FeedbackController::class, 'getOne']);
Route::post('/input_feedback/store/{id_antrian}', [FeedbackController::class, 'input_feedback'])->name('store-feedback');

// route proses antrian
// Route::get('proses_antrian', [ProsesAntrianController::class, 'getone']);
Route::get('proses_antrian/insert', [ProsesAntrianController::class, 'insert']);
Route::get('proses_antrian/update', [ProsesAntrianController::class, 'update']);
Route::get('proses_antrian/delete', [ProsesAntrianController::class, 'delete']);
Route::post('nomor_antrian/{id_antrian}', [ProsesAntrianController::class, 'show']);

// route antrian
// Route::get('petugas/antrian', [AntrianController::class, 'index']);
// Route::post('antrian/insert', [AntrianController::class, 'insert']);


// ROUTE ROLE ADMIN
// Route::get('admin/index', [PenggunaController::class, 'index'])->name('home-admin')->middleware('auth');
// Route::group(['middleware' => ['auth:klien']], function () {
// Route::middleware(['auth', 'klien'])->group(function () {
    Route::get('/home', function () {
        return redirect()->route('home-admin');
    });
    //route profil
    Route::get('/admin/profil', [HomeController::class, 'profil']);
    Route::post('/admin/profil/update', [HomeController::class, 'updateProfil'])->name('update-profil-klien');

    //route pengguna
    Route::get('admin/index', [PenggunaController::class, 'index'])->name('home-admin');
    Route::get('admin/data', [PenggunaController::class, 'data'])->name('admin-data');
    Route::get('admin/pengguna/insert', [PenggunaController::class, 'insert']);
    Route::post('admin/pengguna/store', [PenggunaController::class, 'store'])->name('insert-pengguna');
    Route::delete('admin/pengguna/delete/{idtemporary}', [PenggunaController::class, 'destroy']);
    Route::get('admin/pengguna/edit/', [PenggunaController::class, 'edit']);
    Route::post('admin/pengguna/update/{idtemporary}', [PenggunaController::class, 'update']);
    Route::get('admin/pengguna/search', [PenggunaController::class, 'search'])->name('search');
    Route::get('admin/pengguna/search', [PenggunaController::class, 'dataLayanan'])->name('data-layanan-pengguna');

    //route utk mengelola header footer
    Route::get('/admin/header-footer', [HeaderFooterController::class, 'index']);
    Route::post('/update-header-footer', [HeaderFooterController::class, 'updateHeaderFooter']);

    //route konfigurasi klien
    Route::get('/admin/konfigurasi_klien', [KonfigurasiKlienController::class, 'index']);

    //route layanan admin
    Route::get('/admin/layanan_admin', [LayananAdminController::class, 'index']);
    Route::post('/admin/layanan_admin/insert', [LayananAdminController::class, 'input_klien_layanan']);
    Route::post('/layanan_admin/delete/{id_klien_layanan}', [LayananAdminController::class, 'hapus'])->name('delete-klien-layanan');
    Route::post('/layanan_admin/update/{id_klien_layanan}', [LayananAdminController::class, 'update_klien_layanan']);
    Route::get('/layanan_admin/data-layanan', [LayananAdminController::class, 'dataLayanan'])->name('data-layanan-admin');

    // route klien color
    Route::get('/admin/klien_color', [KlienColorController::class, 'klien_color_getone']);
    Route::get('klien-color/hapus/{id_klien_color}', [KlienColorController::class, 'delete']);
// });

// ROUTE ROLE PETUGAS
// Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', function () {
        return redirect()->route('pemanggil');
    });
    //route pemanggilan antrian
    // Route::resource('/pemanggilan', PemanggilanController::class);
    Route::get('petugas/pemanggilan', [PemanggilanController::class, 'index'])->name('pemanggil');
    Route::get('petugas/pemanggilan/{id_klien_layanan}', [PemanggilanController::class, 'edit']);
    Route::get('petugas/pemanggilan/panggil/antrian', [PemanggilanController::class, 'update_status'])->name('antrian_selanjutnya');
    Route::get('petugas/pemanggilan/panggil/lewati', [PemanggilanController::class, 'status_dilewati'])->name('antrian_dilewati');

    // route profile pengguna
    Route::get('/petugas/profile', [PenggunaController::class, 'profile']);
    Route::post('/profile/update', [PenggunaController::class, 'updateProfile'])->name('update-profile-pengguna');

    //Route untuk pengunjung offline
    Route::get('pengunjung', [PengunjungController::class, 'index']);
    Route::get('pengunjung/daftar/{id_klien_layanan}', [PengunjungController::class, 'form_daftar']);
    Route::post('pengunjung/daftar/store/{id_klien_layanan}', [PengunjungController::class, 'daftar']);
    Route::get('pengunjung/antrian', [PengunjungController::class, 'antrian']);
    Route::get('pengunjung/antrian/{id_antrian}', [PengunjungController::class, 'cetak_pdf']);

    // route antrian pengguna
    Route::get('petugas/antrian_pengguna', [AntrianPenggunaController::class, 'index'])->name('antrean.index');
    Route::get('select', [AntrianPenggunaController::class, 'select2'])->name('antrean.select');
    Route::post('antrian_pengguna/store', [AntrianPenggunaController::class, 'store']);
    // Route::get('antrian_pengguna/input', [AntrianPenggunaController::class, 'insert']);
// });
